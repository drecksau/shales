jQuery('document').ready(function($){

    $('input[type=checkbox]').each(function () {
        if($(this).is(':checked')) {
            var parent_id = $(this).parents('.col-md-12').attr('id');
            $('.title-'+parent_id).slideToggle();
            $('.price-'+parent_id).slideToggle();
        }
        $(this).click(function(){
            var parent_id = $(this).parents('.col-md-12').attr('id');
            $('.title-'+parent_id).slideToggle();
            $('.price-'+parent_id).slideToggle();
        });
    });

});

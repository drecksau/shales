<?php

namespace App\Miscellaneous;
use Illuminate\Support\Facades\Http;
use App\Models\Rate;


class PrivatBankApi
{
    public static function getRates(){
        $response = Http::get('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5');
        if ($response->status() !== 200){
            return;
        }
        $ratesArr=json_decode($response->body());

        foreach ($ratesArr as $ratesItem){
            if (Rate::where('ccy',$ratesItem->ccy)->where('base_ccy',$ratesItem->base_ccy)->get()->count()===0){
                $rate=new Rate();
                $rate->ccy=$ratesItem->ccy;
                $rate->base_ccy=$ratesItem->base_ccy;
                $rate->buy=$ratesItem->buy;
                $rate->sell=$ratesItem->sale;
                $rate->save();
            }
            else{
                Rate::where('ccy',$ratesItem->ccy)->where('base_ccy',$ratesItem->base_ccy)->update(['buy'=>$ratesItem->buy,'sell'=>$ratesItem->sale]);
            }

        }
    }

    private static function saveRates(){

    }

}

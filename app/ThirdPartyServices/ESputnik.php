<?php
/**
 * Created by PhpStorm.
 * User: shmue
 * Date: 07.02.2021
 * Time: 20:33
 */

namespace App\ThirdPartyServices;


class ESputnik
{

    private function sendDetailsMail($orderDetails, $orderId)
    {
        $message_id = env('ESPUTNIK_EMAIL_PUCHASE_DETAILS');
        $user = env('ESPUTNIK_USER');
        $password = env('ESPUTNIK_PASSWORD');
        $send_message_url = 'https://esputnik.com/api/v1/message/' . $message_id . '/smartsend';

        $json_value = new \stdClass();
        $json_value->recipients = [

//                'email' => $orderDetails->email,
            'email' => 'shmuel.livshits@gmail.com',
            'jsonParam' => json_encode([
                'fname' => ucfirst($orderDetails->fName),
                "lName" => ucfirst($orderDetails->lName),
                'sum' => $orderDetails->fullSum,
                'shipping_adress' => $orderDetails->shipping_address,
                'products' => $this->generateProductsForEmail($orderDetails->products),
                'orderId' => strval(intval($orderId)),
            ]),
        ];
//        dd($json_value);
        // В подготовленном сообщении можно использовать передаваемое значение "discount" для каждого контакта, обратившись к нему таким образом: $data.get('discount')
        // Есть возможность передавать массивы объектов и строить контент сообщения с использованием циклов
        return $this->send_email_request($send_message_url, $json_value, $user, $password);
    }

    private function generateProductsForEmail($rawProducts)
    {
        $preparedProducts = [];
        foreach ($rawProducts as $rawProduct) {
            $preparedProduct = new \StdClass();
            $dash = strpos($rawProduct->sku, '_');
            $vendor_code = substr($rawProduct->sku, 0, $dash);
            $vendor_code2 = substr($rawProduct->sku, $dash + 1);
            $shawl = Shawl::where('vendor_code', $vendor_code)->first()->toArray();
            $productInfo = json_decode($shawl['product_info']);
            $preparedProductInfo = [];
            foreach ($productInfo as $item) {
                if ($vendor_code2 === $item->vendor_code2) {
                    $preparedProductInfo = $item;
                }
            }
            $image = (json_decode($productInfo[0]->images))[0];
            $image = str_replace('webp', 'jpg', $image);
            $preparedProduct->image = 'https://back.alefbetsilk.com/' . $image;
            $preparedProduct->name = $shawl['name'];
            $preparedProduct->fabric = Shawl_fabric_structure::getFabricDetails($shawl['shawl_fabric_structure_id'])['name'];
            $preparedProduct->color = Color::getColors(intval($preparedProductInfo->color))['ru'];
            $preparedProduct->price = $preparedProductInfo->price;
            array_push($preparedProducts, $preparedProduct);
        }
        return $preparedProducts;
    }

    private function send_email_request($url, $json_value, $user, $password)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_value));
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $user . ':' . $password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

}
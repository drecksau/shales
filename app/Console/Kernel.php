<?php

namespace App\Console;

use App\Miscellaneous\PrivatBankApi;
use App\Nest\Service\DbToFileWizard;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function (){
            $clientDb= new DbToFileWizard();
            $clientDb->getDb();
            $clientDb->getPopularItemsDb();
            $clientDb->getNav();
            $clientDb->getBlog();
        })->everyMinute()->name('generateDbFile');

        $schedule->call(function (){
            PrivatBankApi::getRates();
        })->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

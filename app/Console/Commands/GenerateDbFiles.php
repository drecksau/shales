<?php

namespace App\Console\Commands;

use App\Nest\Service\DbToFileWizard;
use Illuminate\Console\Command;

class GenerateDbFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generateDbFiles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dbFileWizard= new DbToFileWizard();
        $dbFileWizard->getDb();
        $dbFileWizard->getPopularItemsDb();
    }
}

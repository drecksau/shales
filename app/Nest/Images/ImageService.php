<?php

namespace App\Nest\Images;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;


class ImageService
{
    private const DISK = 'this_project';
    public const PATH='uploads/imgs/';
    private $watermarkPath;

    public function __construct()
    {
        $this->watermarkPath=public_path('/uploads/other/watermark.png');
    }

    public function prepareItemImages($sourceImg, $imageName, $imagesProps, $watermarkProps = null)
    {
       $originalExtension= $this->saveOriginal($sourceImg,$imageName);
        foreach ($imagesProps as $imageProperty) {
            $rightSizeImage = Image::make($sourceImg)->resize($imageProperty['width'], $imageProperty['height'], function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            if ($watermarkProps !== null) {
                $waterMarkWidth=$imageProperty['height'] * $watermarkProps['width'];
                $this->insertWatermark($waterMarkWidth,$rightSizeImage);
            }
            $tinifyImage = new TinifyImages();
            $minifiedImage = $tinifyImage->compressImage($rightSizeImage);

            $filename = $imageProperty['shortName'] . strtolower($imageName);
            Storage::disk($this::DISK)->put($filename.'.'.$originalExtension, $minifiedImage->stream($originalExtension));
            if(function_exists('imagewebp')) {
                Storage::disk($this::DISK)->put($filename . '.webp', $minifiedImage->stream('webp'));
            }
        }
        return $this::PATH.$filename.'.'.$originalExtension;
    }

    public function uploadFileToDisk($file, $attribute_name, $newFileName, $imagesProps)
    {
        if (request()->hasFile($attribute_name) && request()->file($attribute_name)->isValid()) {
            $extension = $file->getClientOriginalExtension();
            if ($extension === 'svg') {
                $newFileName = $newFileName . '.' . $extension;
                Storage::disk($this::DISK)->put($newFileName, file_get_contents($file));
            } else {

                $this->prepareItemImages( $file, $newFileName, $imagesProps);
                $newFileName = $newFileName . '.' . $extension;
            }
            return  $this::PATH. $newFileName;
        }
    }

    public function removeImageIfExists($name,$imagesProps,$path = ''){
        foreach ($imagesProps as $imageProperty) {
            $filename = $imageProperty['shortName'] . strtolower($name);
            Storage::disk($this::DISK)->delete([$path . $filename.'.jpg', $path . $filename.'.jpeg', $path . $filename.'.png', $path . $filename.'.webp', $path . $filename.'.svg']);
        }
    }

    private function saveOriginal($originalImage,$imageName){
        $originalImage= Image::make($originalImage);
        $extension=$this->getExtensionFromMime($originalImage->mime());
        Storage::disk($this::DISK)->put('/originals/'.$imageName.'.'.$extension,$originalImage->stream($extension));
        return $extension;
    }

    private function insertWatermark($width,$image){
        $watermarkOrigin = Image::make($this->watermarkPath);
        $watermark = $watermarkOrigin->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $image->insert($watermark, 'bottom', 0, 20);
    }

    private function getExtensionFromMime($mime){
        $slash=strpos($mime,'/');
        $extension=substr($mime,$slash+1);
        return $extension;
    }


}

<?php

namespace App\Nest;


use App\Models\Category;
use App\Models\Char;
use App\Models\Cloth;
use App\Models\Color;
use App\Models\Letter;
use App\Models\Product;
use App\Models\Size;
use App\Models\SizeCloth;
use App\Models\Tag;
use Illuminate\Support\Facades\Schema;
class ItemsSelection
{
    private $mappingArr;

    public function __construct()
    {
        $this->mappingArr = [
            '$n' => 'name',
            'symb'=>'letter',
            'consist'=>'consist',
            'color'=>'color',
            'sku' => 'sku',
            '$' => 'price',
            '$q' => 'quantity',
            'gift' => 'isPresent',
            'pop' => 'popularity',
            'lft'=>'lft',
            'sex'=>'sex',
            'slug'=>'slug',
            'w'=>'width',
            'h'=>'height',
            'vis'=>'published',
        ];

    }

    public function getItems($obj, $dataUpdateTime = null, array $ids = [],
                             bool $isFullInfo = false)
    {
        if (count($ids) > 0) {
            $items = $obj::whereIn('id', $ids)->get();
        } elseif ($dataUpdateTime === null) {
            if (Schema::hasColumn($obj->getTable(),'published')){
                $items = $obj::where('published',true)->get();
            }
            else{
                $items = $obj::all();
            }

        } else {

                $items = $obj::where('updated_at', '>=', $dataUpdateTime)->get();

        }
        return $this->getItemsCommon($items, $obj, $isFullInfo);
    }

    public function getProducts($obj, $dataUpdateTime = null, array $ids = [], bool $isFullInfo = false)
    {
        if (count($ids) > 0) {
            $items = $obj::whereIn('id', $ids)
//                ->orderBy('popularity', 'desc')->get();
                ->orderBy('id')->get();
        } elseif ($dataUpdateTime === null) {
            $items = $obj::where('published', true)
//                ->orderBy('popularity', 'desc')->get();
                ->orderBy('id')->get();

        } else {
            $items = $obj::where(
                [
                    ['updated_at', '>=', $dataUpdateTime],
                    ['published', true]
                ])
                ->orderBy('id')->get();
        }
        return $this->getItemsCommon($items, $obj, $isFullInfo);
    }

    public function getItemsCommon($items, $selectedObject, $isFullInfo)
    {
        $preparedItems = [];
        foreach ($items as $item) {
            $preparedItem = [];
            $preparedItem['id'] = $selectedObject::ID_PREFIX . $item['id'];
            foreach ($this->mappingArr as $key => $value) {
                if (isset($item[$value]) && $item[$value] != false && gettype($item[$value])!=='array' && gettype($item[$value])!=='object') {
                    $preparedItem[$key] = $item[$value];
                }
            }

            /* if (isset($preparedItem['prices'])){
                 $preparedItem['prices'] = [
                     'old' => $item['old_price'],
                     'ends' => $item['end_sale'],
                     'whole' => [
                         'price' => $item['whole_price'],
                         'qty' => $item['whole_quantity'],
                     ]
                 ];
             }*/
            if (isset($item->tags) && !$item->tags->isEmpty()) {
                $preparedItem[Tag::CLIENT_FILE_ID] = Tag::prepareIdsForClient($item->tags->all());
            }

            $dirs = ItemsSelection::getAttrIds($items, $item, new Category());
            $colors=ItemsSelection::getAttrIds($items,$item, new Color());

            /*$productsRecom = $this->getLinked($item, 'products_recom');
            $sizeProd = $this->getLinked($item, 'products_size');
            $sizeClothes = $this->getLinked($item, 'products_size_clothes');
            if ($productsRecom !== null) {
                $preparedItem[Product::LINKED_PRODUCTS]['rec'] = $productsRecom;
            }
            if ($sizeProd !== null) {
                $preparedItem[Product::LINKED_PRODUCTS]['sd'] = $sizeProd;
            }
            if ($sizeClothes !== null) {
                $preparedItem[Product::LINKED_PRODUCTS]['sc'] = $sizeClothes;
            }*/

            if ($dirs !== null) {
                $preparedItem[Category::CLIENT_FILE_ID] = $dirs;
            }
            if ($colors !== null) {
                $preparedItem[Color::CLIENT_FILE_ID] = $colors;
            }

            if (isset($item['size_id'])) {
                $preparedItem[Size::CLIENT_FILE_ID] = Size::ID_PREFIX . $item['size_id'];
            }
            if (isset($item['letter_id'])) {
                $preparedItem[Letter::CLIENT_FILE_ID] = Letter::ID_PREFIX . $item['letter_id'];
            }
            if (isset($item['size_clothes_id'])) {
                $preparedItem[SizeCloth::CLIENT_FILE_ID] = SizeCloth::ID_PREFIX . $item['size_clothes_id'];
            }
            if (isset($item['cloth_id'])) {
                $preparedItem[Cloth::CLIENT_FILE_ID] = Cloth::ID_PREFIX . $item['cloth_id'];
            }
            if (!empty($item['poster']) && file_exists($item['poster'])) {
                $preparedItem['$i'][] = ItemsSelection::getImageExtension($item['poster']);
            }
            if (!empty($item['images'])) {
                foreach ($item['images'] as $k=>$image) {
                    $image_url = ltrim($image->image, '/');
                    if(!empty($image->image) && file_exists($image_url)) {
                        $preparedItem['$i'][] = ItemsSelection::getImageExtension($image->image);
                    }
                }
            }
            if (!empty($item['cover'])) {
                $preparedItem['cover'] = ItemsSelection::getImageExtension($item['cover']);
            }
            if ($item::CLIENT_FILE_ID === Category::CLIENT_FILE_ID) {
                $preparedItem['chars'] = $this->getCategoryChars($item->chars);
            }
            if (isset($item['parent_id']) && $item['parent_id'] !== 0) {
                $preparedItem['parent'] = Category::ID_PREFIX . $item['parent_id'];
            }


            $pageTitle = null;
            if ($isFullInfo == true) {
                $pageTitle = $item['meta_title'];
                $preparedItem['seo'] = [
                    '$t' => $pageTitle,
                ];
                if (isset($item['description'])) {
                    $preparedItem['$d'] = $item['description'];
                }
                if ($item['meta_description']) {
                    $pageDescr = $item['meta_description'];
                    $preparedItem['seo']['$d'] = $pageDescr;
                }
            }
            $preparedItems[$selectedObject::ID_PREFIX . $item['id']] = $preparedItem;
        }
        return $preparedItems;
    }

    private function getCategoryChars($chars)
    {
        $chars = json_decode($chars);
        $readyChars = [];
        if ($chars !== null) {
            foreach ($chars as $dirChar) {
                $char = Char::where('id', intval($dirChar->char))->first();
                if ($char !== null) {
                    $readyChar = [
                        'name' => $char->title,
                        'type' => $char->type
                    ];
                    if (boolval($dirChar->chars_front) === true) {
                        $readyChar['main'] = 1;
                    }
                    if (boolval($dirChar->chars_filter) === true) {
                        $readyChar['filter'] = 1;
                    }
                    if (!empty($char->suffix)) {
                        $readyChar['suf'] = $char->suffix;
                    }
                    $readyChars[$char->code_name] = $readyChar;
                }

            }

            return $readyChars;
        }
        return null;
    }

    private function getImageExtension($imagePath)
    {
        $lastDot = strrpos($imagePath, '.');
        $extension = substr($imagePath, $lastDot + 1);
        return $extension;
    }

    private function getAttrIds($products, $product, $attr)
    {
        $att1 = new $attr();
        $className = $att1->getClass();

        $items = $products->find($product['id'])->{$className};
        if ($items === null) {
            return null;
        }
        $ids = [];
        foreach ($items as $item) {
            $id = $att1::ID_PREFIX . $item['id'];
            array_push($ids, $id);
        }
        return ($ids === []) ? null : $ids;
    }

    private function getLinked($product, $linkedType)
    {
        $items = null;
        if ($linkedType === 'products_size') {
            $items = $product->products_size;
        }
        if ($linkedType === 'products_size_clothes') {

            $items = $product->products_size_clothes;
        }
        if ($linkedType === 'products_recom') {

            $items = $product->products_recom;
        }
        $ids = [];
        if ($items !== null) {
            foreach ($items as $item) {
                if ($item->published==true){
                    array_push($ids, Product::ID_PREFIX . $item->id);
                }
            }
            if ($linkedType === 'products_size_clothes' && $ids !== []) {

                array_push($ids, Product::ID_PREFIX . $product->id);
            }

            if ($linkedType === 'products_size' && $ids !== []) {

                array_push($ids, Product::ID_PREFIX . $product->id);
            }

        }
        $ids = array_unique($ids);
        return ($ids === []) ? null : $ids;

    }

    private function sortByVolsDesc($connectionsVols)
    {
        $volsArr = collect([]);
        foreach ($connectionsVols as $item) {
            $idNumber = (int)filter_var($item, FILTER_SANITIZE_NUMBER_INT);
            $itemVol = Product::where('id', $idNumber)->first('volume')->volume;
            $volsArr[$item] = $itemVol;
        }
        $sortedItems = $volsArr->sort();

        return $sortedItems->keys()->toArray();


    }


    public static function getFromProducts($products, $obj, string $clientFileId)
    {
        $prods = collect($products);
        if ($prods->isEmpty()) {
            return null;
        }
        $ids = collect([]);
        foreach ($prods as $prod) {
            if (isset($prod[$clientFileId])) {
                if (gettype($prod[$clientFileId]) === 'string') {//если один к одному
                    $id = (int)filter_var($prod[$clientFileId], FILTER_SANITIZE_NUMBER_INT);
                    $ids->add($id);
                } elseif (gettype($prod[$clientFileId]) === "array") {//если один ко многим
                    foreach ($prod[$clientFileId] as $singleID) {
                        $id = (int)filter_var($singleID, FILTER_SANITIZE_NUMBER_INT);
                        $ids->add($id);
                    }
                }
            }

        }

        if ($ids->count() !== 0) {
            $ids = $ids->unique();

            return $obj::getItems(null, $ids->toArray());
        }

    }

    public static function getNamesList(array $items = [])
    {
        if (count($items) === 0) {
            return null;
        }
        $names = '';
        foreach ($items as $key => $item) {
            $names .= $item->name;
            if ($key!==count($items) - 1)
            $names .= ', ';
        }
        return $names;
    }

}

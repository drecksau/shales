<?php

namespace App\Nest;


use App\Models\CatArticle;
use App\Models\TagArticle;
use Illuminate\Support\Facades\Schema;

class BlogSelection
{
    private $mappingArr;

    public function __construct()
    {
        $this->mappingArr = [
            '$n' => 'name',
            '$' => 'price',
            'lft'=>'lft',
            'sex'=>'sex',
            'slug'=>'slug',
            'vis'=>'published',
        ];

    }

    public function getItems($obj, $dataUpdateTime = null, array $ids = [],
                             bool $isFullInfo = false)
    {
        if (count($ids) > 0) {
            $items = $obj::whereIn('id', $ids)->get();
        } elseif ($dataUpdateTime === null) {
            if (Schema::hasColumn($obj->getTable(),'published')){
                $items = $obj::where('published',true)->get();
            }
            else{
                $items = $obj::all();
            }

        } else {

            $items = $obj::where('updated_at', '>=', $dataUpdateTime)->get();

        }
        return $this->getItemsCommon($items, $obj, $isFullInfo);
    }

    public function getItemsCommon($items, $selectedObject, $isFullInfo)
    {
        $preparedItems = [];
        foreach ($items as $item) {
            $preparedItem = [];
            $preparedItem['id'] = $selectedObject::ID_PREFIX . $item['id'];
            foreach ($this->mappingArr as $key => $value) {
                if (isset($item[$value]) && $item[$value] != false && gettype($item[$value])!=='array' && gettype($item[$value])!=='object') {
                    $preparedItem[$key] = $item[$value];
                }
            }
            if (isset($item->tags) && !$item->tags->isEmpty()) {
                $preparedItem[TagArticle::CLIENT_FILE_ID] = TagArticle::prepareIdsForClient($item->tags->all());
            }

            $dirs = BlogSelection::getAttrIds($items, $item, new CatArticle());

            if ($dirs !== null) {
                $preparedItem[CatArticle::CLIENT_FILE_ID] = $dirs;
            }

            if (!empty($item['poster']) && file_exists($item['poster'])) {
                $preparedItem['$i'][] = BlogSelection::getImageExtension($item['poster']);
            }
            if (!empty($item['images'])) {
                foreach ($item['images'] as $k=>$image) {
                    $image_url = ltrim($image->image, '/');
                    if(!empty($image->image) && file_exists($image_url)) {
                        $preparedItem['$i'][] = BlogSelection::getImageExtension($image->image);
                    }
                }
            }
            if (!empty($item['cover'])) {
                $preparedItem['cover'] = BlogSelection::getImageExtension($item['cover']);
            }
            if (isset($item['parent_id']) && $item['parent_id'] !== 0) {
                $preparedItem['parent'] = CatArticle::ID_PREFIX . $item['parent_id'];
            }


            $pageTitle = null;
            if ($isFullInfo == true) {
                $pageTitle = $item['meta_title'];
                $preparedItem['seo'] = [
                    '$t' => $pageTitle,
                ];
                if (isset($item['description'])) {
                    $preparedItem['$d'] = $item['description'];
                }
                if ($item['meta_description']) {
                    $pageDescr = $item['meta_description'];
                    $preparedItem['seo']['$d'] = $pageDescr;
                }
            }
            $preparedItems[$selectedObject::ID_PREFIX . $item['id']] = $preparedItem;
        }
        return $preparedItems;
    }

    private function getImageExtension($imagePath)
    {
        $lastDot = strrpos($imagePath, '.');
        $extension = substr($imagePath, $lastDot + 1);
        return $extension;
    }

    private function getAttrIds($entities, $entity, $attr)
    {
        $att1 = new $attr();
        $className = $att1->getClass();

        $items = $entities->find($entity['id'])->{$className};
        if ($items === null) {
            return null;
        }
        $ids = [];
        foreach ($items as $item) {
            $id = $att1::ID_PREFIX . $item['id'];
            array_push($ids, $id);
        }
        return ($ids === []) ? null : $ids;
    }

    public static function getNamesList(array $items = [])
    {
        if (count($items) === 0) {
            return null;
        }
        $names = '';
        foreach ($items as $key => $item) {
            $names .= $item->name;
            if ($key!==count($items) - 1)
                $names .= ', ';
        }
        return $names;
    }

}

<?php

namespace App\Nest\Service;


use App\Models\Article;
use App\Models\CatArticle;
use App\Models\Category;
use App\Models\Char;
use App\Models\Cloth;
use App\Models\Color;
use App\Models\Letter;
use App\Models\MenuItemF;
use App\Models\MenuItemL;
use App\Models\MenuItemR;
use App\Models\Product;
use App\Models\Site_page;
use App\Models\Size;
use App\Models\SizeCloth;
use App\Models\Tag;
use App\Models\TagArticle;
use App\Models\Variable;
use App\Nest\ItemsSelection;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class DbToFileWizard
{
    public const dbFilePath = 'temp';
    public const dirName = '/ClientFiles/';
    public const fileName = 'db.txt';
    public const blogName = 'blog.txt';
    public const navName = 'nav.txt';
    public const initFileName = 'init.txt';
    public $dbFilePathes;
    private $maxQuantity = 100;

    public function __construct()
    {
        foreach (config('app.available_locales') as $locale) {
            $this->dbFilePathes[$locale] = DbToFileWizard::dirName . $locale;
        }
    }

    public function getNav(){
        foreach ($this->dbFilePathes as $lang => $filePath) {
            app()->setLocale($lang);
            $navFileName = $filePath . '/' . DbToFileWizard::navName;
            $fileUpdateTime = $this->getDbFileUpdateTime($navFileName);
            $MenuItemL=MenuItemL::getItems($fileUpdateTime);
            $MenuItemR=MenuItemR::getItems($fileUpdateTime);
            $MenuItemF=MenuItemF::getItems($fileUpdateTime);
            $mainArray['header']['nav'] = array_merge(
                $MenuItemL,
                $MenuItemR
            );
            $mainArray['footer']['nav'] = $MenuItemF;

            $vars=Variable::getItems('footer');
            if($vars) $mainArray['footer']['vars'] = $vars;


            $isExist = Storage::exists($navFileName);
            if ($isExist) {
                $fileArray = Storage::get($navFileName);
                if ($fileArray !== null) {
                    $fileArray = json_decode($fileArray, true);
                    foreach ($mainArray['header']['nav'] as $key => $value) {
                        if (isset($fileArray['header']['nav'][$key])) {
                            $fileArray['header']['nav'][$key] = $value;
                        } else {
                            $fileArray['header']['nav']->put($key, $value);
                        }
                    }

                    foreach ($mainArray['footer']['nav'] as $key => $value) {
                        if (isset($fileArray['footer']['nav'][$key])) {
                            $fileArray['footer']['nav'][$key] = $value;
                        } else {
                            $fileArray['footer']['nav']->put($key, $value);
                        }
                    }

                    foreach ($mainArray['footer']['vars'] as $key => $value) {
                        if (isset($fileArray['footer']['vars'][$key])) {
                            $fileArray['footer']['vars'][$key] = $value;
                        } else {
                            $fileArray['footer']['vars']->put($key, $value);
                        }
                    }

                }
                Storage::put($navFileName, json_encode($fileArray));
            } else {
                Storage::put($navFileName, json_encode($mainArray));
            }
        }
    }

    public function getBlog()
    {
        foreach ($this->dbFilePathes as $lang => $filePath) {
            app()->setLocale($lang);
            $fullFileName = $filePath . '/' . DbToFileWizard::blogName;
            $fileUpdateTime = $this->getDbFileUpdateTime($fullFileName);
            $article = new Article();

            $items = $article->getItems($fileUpdateTime);
            $categories = CatArticle::getItems($fileUpdateTime);
            $tags=TagArticle::getItems($fileUpdateTime);

            $mainArray = array_merge(
                $categories,
                $tags,
                $items
            );
            $isExist = Storage::exists($fullFileName);
            if ($isExist) {
                $fileArray = Storage::get($fullFileName);
                if ($fileArray !== null) {
                    $fileArray = collect(json_decode($fileArray));
                    foreach ($mainArray as $key => $value) {
                        if (isset($fileArray[$key])) {
                            $fileArray[$key] = $value;
                        } else {
                            $fileArray->put($key, $value);
                        }
                    }
                }
                $fileArray = $fileArray->sortBy('lft');
                Storage::put($fullFileName, json_encode($fileArray));
            } else {
                Storage::put($fullFileName, json_encode($mainArray));
            }

        }

    }
    /**
     * взять все товары и все зависимости
     */
    public function getDb()
    {
        foreach ($this->dbFilePathes as $lang => $filePath) {
            app()->setLocale($lang);
            $fullFileName = $filePath . '/' . DbToFileWizard::fileName;
            $fileUpdateTime = $this->getDbFileUpdateTime($fullFileName);
            $product = new Product();

            $items = $product->getItems($fileUpdateTime);
            $letters=Letter::getItems($fileUpdateTime);
            $tags = Tag::getItems($fileUpdateTime);
            $cloth = Cloth::getItems($fileUpdateTime);
            $sizes=Size::getItems($fileUpdateTime);
            $sizeCloth=SizeCloth::getItems($fileUpdateTime);

            $categories = Category::getItems($fileUpdateTime);
            $sitePages=Site_page::getItems($fileUpdateTime);
            $mainArray = array_merge(
                $letters,
                $tags,
                $categories,
                $sitePages,
                $cloth,
                $sizes,
                $sizeCloth,
                $items
            );
            $isExist = Storage::exists($fullFileName);
            if ($isExist) {
                $fileArray = Storage::get($fullFileName);
                if ($fileArray !== null) {
                    $fileArray = collect(json_decode($fileArray));
                    foreach ($mainArray as $key => $value) {
                        if (isset($fileArray[$key])) {
                            $fileArray[$key] = $value;
                        } else {
                            $fileArray->put($key, $value);
                        }
                    }
                }
                $fileArray = $fileArray->sortBy('lft');
                Storage::put($fullFileName, json_encode($fileArray));
            } else {
                Storage::put($fullFileName, json_encode($mainArray));
            }

        }

    }

    public function getPopularItemsDb()
    {
        foreach ($this->dbFilePathes as $lang => $filePath) {
            $fullFileName = $filePath . '/' . DbToFileWizard::initFileName;
            $productsObj = new Product();
            $popularItems = $productsObj->getPopularProductsIdsArr($this->maxQuantity);
            if ($popularItems !== null) {
                $popularItemsReady = $productsObj->getItems(null, $popularItems->toArray());
                $mainArray = $this->getPropsFromProducts($popularItemsReady);
                $dbObjStr = json_encode($mainArray);
                Storage::put($fullFileName, $dbObjStr);
            }
        }
    }

    /**
     *init найти самые популярные x товаров и подвязать зависимости
     */
    public function getObj(string $id)
    {
        $typeId = preg_replace("/[^a-z]+/", "", $id);
        $idNumber = (int)filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $product = new Product();
        $item = null;
        switch ($typeId) {
            case Category::ID_PREFIX :
                $item = Category::getItems(null, [$idNumber], true);
                break;
            case Product::ID_PREFIX :
                $item = $product->getItems(null, [$idNumber], true);
                break;
            case Site_page::ID_PREFIX :
                $item = Site_page::getItems(null, [$idNumber], true);
                break;
            case SizeCloth::ID_PREFIX :
                $item = SizeCloth::getItems(null, [$idNumber], true);
                break;
            case Size::ID_PREFIX :
                $item = Size::getItems(null, [$idNumber], true);
                break;
            case Letter::ID_PREFIX :
                $item = Letter::getItems(null, [$idNumber], true);
                break;
            case Color::ID_PREFIX :
                $item = Color::getItems(null, [$idNumber], true);
                break;
            case Cloth::ID_PREFIX :
                $item = Cloth::getItems(null, [$idNumber], true);
                break;
            case Tag::ID_PREFIX :
                $item = Tag::getItems(null, [$idNumber], true);
                break;

            case Article::ID_PREFIX :
                $item = Article::getItems(null, [$idNumber], true);
                break;
            case CatArticle::ID_PREFIX :
                $item = CatArticle::getItems(null, [$idNumber], true);
                break;
            case TagArticle::ID_PREFIX :
                $item = TagArticle::getItems(null, [$idNumber], true);
                break;
            default:
                return 'wrong-id';
        }
        $item = array_values($item);
        return (count($item) === 1) ? $item[0] : 'wrong-id';

    }

    /**
     * по товарам берез из базы все зависимости и присоединяет их к товарам. Товары нужно использовать после Product::getItems
     */
    public function getPropsFromProducts($products, bool $getConnectionProducts = false, $isPages = false, $id = null)
    {
        $mainArray = [];
        if ($isPages){
            $id=(int)filter_var($id, FILTER_SANITIZE_NUMBER_INT);
            $pages=Site_page::getItems(null,[$id],true);
            $fullFileName = DbToFileWizard::dirName.app()->getLocale() . '/' . DbToFileWizard::initFileName;
            $fileArray = Storage::get($fullFileName);
            $fileArray=collect(json_decode($fileArray));
            $mainArray=$fileArray->merge($pages);
            $mainArray = $mainArray->sortBy('lft');
            return $mainArray->toArray();

        }
        else{
            if ($getConnectionProducts === true) {
                $connectionsProductsIdsArray = $this->getConnectionsProductsIdsArray($products);
                if ($connectionsProductsIdsArray !== null) {
                    $productObj = new Product();
                    $linkedProducts = $productObj->getItems(null, $connectionsProductsIdsArray);
                    $products = array_merge($linkedProducts, $products);
                }
            }
            $categories = Category::getItems();
            if ($categories !== null) {
                $mainArray = array_merge($mainArray, $categories);
            }
            $sitePages = Site_page::getItems();
            if ($sitePages !== null) {
                $mainArray = array_merge($mainArray, $sitePages);
            }
            $cloth = ItemsSelection::getFromProducts($products, new Cloth(), Cloth::CLIENT_FILE_ID);
            if ($cloth !== null) {
                $mainArray = array_merge($mainArray, $cloth);
            }
            $color = ItemsSelection::getFromProducts($products, new Color(), Color::CLIENT_FILE_ID);
            if ($color !== null) {
                $mainArray = array_merge($mainArray, $color);
            }
            $letter = ItemsSelection::getFromProducts($products, new Letter(), Letter::CLIENT_FILE_ID);
            if ($letter !== null) {
                $mainArray = array_merge($mainArray, $letter);
            }
            $sizeCloth = ItemsSelection::getFromProducts($products, new SizeCloth(), SizeCloth::CLIENT_FILE_ID);
            if ($sizeCloth !== null) {
                $mainArray = array_merge($mainArray, $sizeCloth);
            }
            $size = ItemsSelection::getFromProducts($products, new Size(), Size::CLIENT_FILE_ID);
            if ($size !== null) {
                $mainArray = array_merge($mainArray, $size);
            }

            $tags = ItemsSelection::getFromProducts($products, new Tag(), Tag::CLIENT_FILE_ID);
            if ($tags !== null) {
                $mainArray = array_merge($mainArray, $tags);
            }
            $mainArray = array_merge($mainArray, $products);
            if ($id !== null) {
                $entity = $this->getObj($id);
                if (gettype($entity)==='array'){
                    $mainArray[$entity['id']] = $entity;
                }

            }
        }
        $mainArray = collect($mainArray);
        $mainArray = $mainArray->sortBy('lft');
        return $mainArray->toArray();

    }

    public function foolInfoForProductOrNot($id)
    {
        $typeId = preg_replace("/[^a-z]+/", "", $id);
        if ($typeId === Product::ID_PREFIX || $typeId === Site_page::ID_PREFIX) {
            return true;
        }
        return false;
    }

    private function getConnectionsProductsIdsArray($products)
    {
        $ids = collect([]);
        /*foreach ($products as $product) {
            if (isset($product['@'])) {
                if (isset($product['@']['vols'])) {
                    $ids = $ids->merge($product['@']['vols']);
                }
                if (isset($product['@']['accs'])) {
                    $ids = $ids->merge($product['@']['accs']);
                }
            }
        }*/
        $idNumbers = $ids->map(function ($id) {
            return (int)filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        });
        return ($idNumbers->isEmpty()) ? null : $idNumbers->toArray();
    }

    public function getProductsFromProps($id)
    {
        $typeId = preg_replace("/[^a-z]+/", "", $id);
        $idNumber = (int)filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $products = null;
        switch ($typeId) {
            case Cloth::ID_PREFIX :
                return Product::where([['cloth_id', $idNumber], ['published', true]])->get('id')->take($this->maxQuantity)->toArray();

            case Product::ID_PREFIX :
                return Product::where([['id', $idNumber], ['published', true]])->get('id')->take($this->maxQuantity)->toArray();
                break;
            case Letter::ID_PREFIX :
                return Product::where([['letter_id', $idNumber], ['published', true]])->get('id')->take($this->maxQuantity)->toArray();
                break;
            case Size::ID_PREFIX :
                return Product::where([['size_id', $idNumber], ['published', true]])->get('id')->take($this->maxQuantity)->toArray();
                break;
            case SizeCloth::ID_PREFIX :
                return Product::where([['size_clothes_id', $idNumber], ['published', true]])->get('id')->take($this->maxQuantity)->toArray();
                break;
            case Category::ID_PREFIX :
                return Category::getItemsInCategory($idNumber, $this->maxQuantity);
                break;

            case Tag::ID_PREFIX :
                return Product::whereHas('tags', function ($q) use ($idNumber) {
                    $q->where('id', $idNumber);
                })->get('id')->take($this->maxQuantity)->toArray();
                break;
            case Color::ID_PREFIX :
                return Product::whereHas('colors', function ($q) use ($idNumber) {
                    $q->where('id', $idNumber);
                })->get('id')->take($this->maxQuantity)->toArray();
                break;

            case Site_page::ID_PREFIX :
                return Site_page::where([['id', $idNumber], ['published', true]])->get('id')->toArray();
                break;
            default:
                return [];
        }
        return [];
    }


    public function getDbFileUpdateTime($path)
    {
        if (Storage::exists($path)) {
            $lastModified = Storage::lastModified($path);
            return Carbon::createFromTimestamp($lastModified, config('timezone'));
        }
        return null;
    }

    public function removeFromClientFiles($clientId)
    {
        foreach ($this->dbFilePathes as $lang => $filePath) {
            $fullFileNames = [$filePath . '/' . DbToFileWizard::fileName,$filePath . '/' . DbToFileWizard::initFileName];
            foreach ($fullFileNames as $fullFileName){
                $fileArray = Storage::get($fullFileName);
                if ($fileArray !== null) {
                    $fileArray = collect(json_decode($fileArray));
                    $fileArray = $fileArray->forget($clientId);
                    Storage::put($fullFileName, json_encode($fileArray));
                }
            }

        }
    }
    public static function updateDbFiles(){
        $dbToFileWizard=new DbToFileWizard();
        $dbToFileWizard->getPopularItemsDb();
        $dbToFileWizard->getDb();
    }
}

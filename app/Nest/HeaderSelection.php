<?php

namespace App\Nest;


use App\Models\Category;
use App\Models\Char;
use App\Models\Cloth;
use App\Models\Color;
use App\Models\Letter;
use App\Models\Product;
use App\Models\Size;
use App\Models\SizeCloth;
use App\Models\Tag;
use Illuminate\Support\Facades\Schema;
class HeaderSelection
{
    private $mappingArr;

    public function __construct()
    {
        $this->mappingArr = [
            '$n' => 'name',
            'lft'=>'lft',
            'parent'=>'parent_id',
            'target'=>'target',
            'link'=>'link',
            'vis'=>'published',
        ];

    }

    public function getItems($obj, $dataUpdateTime = null, array $ids = [],
                             bool $isFullInfo = false)
    {
        if (count($ids) > 0) {
            $items = $obj::whereIn('id', $ids)->get();
        } elseif ($dataUpdateTime === null) {
            if (Schema::hasColumn($obj->getTable(),'published')){
                $items = $obj::where('published',true)->get();
            }
            else{
                $items = $obj::all();
            }

        } else {

                $items = $obj::where('updated_at', '>=', $dataUpdateTime)->get();
        }
        return $this->getItemsCommon($items, $obj, $isFullInfo);
    }


    public function getItemsCommon($items, $selectedObject, $isFullInfo)
    {
        $preparedItems = [];
        foreach ($items as $item) {
            $preparedItem = [];
            $preparedItem['id'] = $selectedObject::ID_PREFIX . $item['id'];
            foreach ($this->mappingArr as $key => $value) {
                if (isset($item[$value]) && $item[$value] != false && gettype($item[$value])!=='array' && gettype($item[$value])!=='object') {
                    $preparedItem[$key] = $item[$value];
                }
            }



            /*if (!empty($item['poster']) && file_exists($item['poster'])) {
                $preparedItem['$i'][] = HeaderSelection::getImageExtension($item['poster']);
            }
            if (!empty($item['images'])) {
                foreach ($item['images'] as $k=>$image) {
                    $image_url = ltrim($image->image, '/');
                    if(!empty($image->image) && file_exists($image_url)) {
                        $preparedItem['$i'][] = HeaderSelection::getImageExtension($image->image);
                    }
                }
            }
            if (!empty($item['cover'])) {
                $preparedItem['cover'] = HeaderSelection::getImageExtension($item['cover']);
            }*/

            $preparedItems[$selectedObject::ID_PREFIX . $item['id']] = $preparedItem;
        }
        return $preparedItems;
    }


    private function getImageExtension($imagePath)
    {
        $lastDot = strrpos($imagePath, '.');
        $extension = substr($imagePath, $lastDot + 1);
        return $extension;
    }

    private function getAttrIds($products, $product, $attr)
    {
        $att1 = new $attr();
        $className = $att1->getClass();

        $items = $products->find($product['id'])->{$className};
        if ($items === null) {
            return null;
        }
        $ids = [];
        foreach ($items as $item) {
            $id = $att1::ID_PREFIX . $item['id'];
            array_push($ids, $id);
        }
        return ($ids === []) ? null : $ids;
    }


    public static function getNamesList(array $items = [])
    {
        if (count($items) === 0) {
            return null;
        }
        $names = '';
        foreach ($items as $key => $item) {
            $names .= $item->name;
            if ($key!==count($items) - 1)
            $names .= ', ';
        }
        return $names;
    }

}

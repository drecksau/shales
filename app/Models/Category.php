<?php

namespace App\Models;

use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use CrudTrait;
    use Sluggable, SluggableScopeHelpers;
    use HasTranslations;


    protected $table = 'categories';
    protected $guarded = ['id'];
    protected $translatable = ['name', 'description', 'meta_title', 'meta_description'];
    public const ID_PREFIX = 'd';
    public const CLIENT_FILE_ID='dirs';

    public function sluggable(): array {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    public static function getItems($dataUpdateTime = null, array $ids = [], bool $isFullInfo = false)
    {
        $itemSelection = new ItemsSelection();
        $categories = $itemSelection->getItems(new Category(), $dataUpdateTime, $ids, $isFullInfo);
        $categories=collect($categories) ->sortBy('lft');
        $categories=$categories->where('vis',1);
        return $categories->toArray() ;
    }

    public static function getItemsInCategory(int $categoryIdNumber, $maxQuantity){
        $category=Category::where('id',$categoryIdNumber)->first();
        $categoryProducts=Product::where('published', true)->whereHas('categories', function ($q) use ($categoryIdNumber) {
            $q->where('id', $categoryIdNumber);
        })->get('id')->take($maxQuantity)->toArray();
        $categoryChildren=$category->children;
        if ($categoryChildren!==null){
            foreach ($categoryChildren as $childrenCategory){
                $childProducts=Product::where('published', true)->whereHas('categories', function ($q) use ($childrenCategory) {
                    $q->where('id', $childrenCategory['id']);
                })->get('id')->take($maxQuantity)->toArray();
                $categoryProducts =array_merge($categoryProducts,$childProducts);
                if (count($categoryProducts) >=$maxQuantity){
                    break;
                }
            }
        }

        return $categoryProducts ;
    }

    public function getClass(){
        return $this->table;
    }


    public function char(){
        return $this->belongsTo(Char::class, 'chars');
    }
    public function parent() {
        return $this->belongsTo(Category::class, 'parent_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use CrudTrait;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function prepareForOutDoor($user)
    {
        $mappingArr = [
            'name',
            'last_name',
            'email',
            'city',
            'phone',
            'address',
            'zip',
            'country',
            'shipping_provider',
            'shipping_destination',
            'payment_mask',
            'payment_method',
            'payment_provider',
            'payment_tocken',
            'news'
        ];
        $mappingArrTypes = [ // в этом массиве отдаються поля в том случае если они пустые
            'name' => '',
            'last_name' => '',
            'email' => '',
            'city' => '',
            'phone' => '',
            'address' => '',
            'zip' => '',
            'country' => '',
            'shipping_provider'=>'',
            'shipping_destination'=>'',
            'payment_mask'=>'',
            'payment_method'=>'',
            'payment_provider'=>'',
            'payment_tocken'=>'',
            'news' => true,
        ];
        $prepUser = new \stdClass();
        foreach ($mappingArr as $key => $value) {

            $defaultValue = (isset($mappingArrTypes[$key])) ? $mappingArrTypes[$key] : $mappingArrTypes[$value];
            $readyValue = (!isset($user->$value)) ? $defaultValue : $user->$value;
            if (gettype($key) === 'integer') {
                $prepUser->$value = $readyValue;
            } else {
                $prepUser->$key = $readyValue;
            }
        }
        $prepUser->news = boolval($prepUser->news);
        return $prepUser;
    }
}

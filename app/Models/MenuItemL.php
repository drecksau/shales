<?php

namespace App\Models;

use App\Nest\HeaderSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class MenuItemL extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $table = 'menu_i_l';
    protected $guarded = ['id'];
    protected $translatable = ['name', 'link'];
    public const ID_PREFIX='LL';

    public function parent() {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public static function getItems( $dataUpdateTime=null, array $ids=[], bool $isFullInfo = false){
        $itemSelection = new HeaderSelection();
        return $itemSelection->getItems(new MenuItemL(),$dataUpdateTime,$ids,$isFullInfo);
    }
}

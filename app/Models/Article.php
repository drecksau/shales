<?php

namespace App\Models;

use App\Nest\BlogSelection;
use App\Nest\Images\ImageService;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Article extends Model
{
    use CrudTrait;
    use HasTranslations;
    use Sluggable, SluggableScopeHelpers;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'articles';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $translatable = ['name', 'description','meta_title','meta_description'];
    public const ID_PREFIX='ba';
    public const CLIENT_FILE_ID='ba';

    public function setPosterAttribute($value)
    {
        $attribute_name = "poster";
        $imagesProductsProps=config('filesystems.disks.this_project.cover_pages');
        $watermarkProps= null; //config('filesystems.disks.this_project.watermark');
        $id=$this->id;
        if ($id===null){//если это добавление нового объекта, а не редактирование - нужно взять в таблице значение слудующего автоинкремента
            $statement = DB::select("SHOW TABLE STATUS LIKE '".$this->table."'");
            $nextId = $statement[0]->Auto_increment;
            $id=$nextId;
        }
        $newName=$this::ID_PREFIX.$id;
        $imageService= new ImageService();

        //dd($newName);
        if ($value==null) {
            $this->attributes[$attribute_name] = null;
            $imageService->removeImageIfExists($newName,$imagesProductsProps);

            return;
        }
        if (!strpos($value,ImageService::PATH)) {// если к нам зашла не картинка а url (так как файл был сохранен ранее то мы ничего не делаем, а если картинка - то заходим в функцию и обрабатываем )
            $imageService->removeImageIfExists($newName,$imagesProductsProps);
            $imageService->removeImageIfExists($newName,$imagesProductsProps, '/originals/');
            $this->attributes[$attribute_name] = $imageService->prepareItemImages($value,$newName,$imagesProductsProps,$watermarkProps);
        }
    }

    public static function getItems($dataUpdateTime=null, array $ids=[], bool $isFullInfo = false){
        $itemSelection= new BlogSelection();
        $sitePages=$itemSelection->getItems(new Article(),$dataUpdateTime,$ids,$isFullInfo);
        $sitePages=collect($sitePages) ->sortBy('lft');
        $sitePages=$sitePages->where('vis',1);
        return $sitePages->toArray();
    }

    public function sluggable(): array {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    public function categories(){
        return $this->belongsToMany(CatArticle::class, 'article_cat_article');
    }
    public function tags() {
        return $this->belongsToMany(TagArticle::class, 'article_tag_article');
    }
}

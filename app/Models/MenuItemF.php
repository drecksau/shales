<?php

namespace App\Models;

use App\Nest\HeaderSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class MenuItemF extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $table = 'menu_i_f';
    protected $guarded = ['id'];
    protected $translatable = ['name', 'link'];
    public const ID_PREFIX='LF';

    public function parent() {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public static function getItems( $dataUpdateTime=null, array $ids=[], bool $isFullInfo = false){
        $itemSelection = new HeaderSelection();
        return $itemSelection->getItems(new MenuItemF(),$dataUpdateTime,$ids,$isFullInfo);
    }

}

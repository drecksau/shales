<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Variable extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $table = 'variable';
    protected $guarded = ['id'];
    public $translatable = ['name'];
    public const ID_PREFIX='V';

    public static function getItems($region){
        $result=[];
        $vars =  Variable::where('code', 'like', $region . '%')->get(['name', 'code'])->toArray();
        if(empty($vars)) return false;
        foreach ($vars as $var) {
            $result[$var['code']]=$var['name'];
        }
        return $result;
    }
    public function getName() {
        return $this->name;
    }

    public function toArray()
    {
        $attributes = parent::toArray();
        foreach ($this->getTranslatableAttributes() as $field) {
            $attributes[$field] = $this->getTranslation($field, App::getLocale());
        }
        return $attributes;
    }
}

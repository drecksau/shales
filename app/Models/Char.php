<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Char extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $table = 'chars';
    protected $guarded = ['id'];
    protected $translatable = ['name', 'suffix'];
}

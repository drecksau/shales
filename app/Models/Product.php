<?php

namespace App\Models;

use App\Nest\Images\ImageService;
use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Product extends Model implements HasMedia
{
    use CrudTrait;
    use Sluggable, SluggableScopeHelpers;
    use HasTranslations;
    use HasMediaTrait;

    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $translatable = ['name', 'description', 'meta_title', 'meta_description'];

    public const ID_PREFIX = 'i';
    public const CLIENT_FILE_ID='i';
    public const LINKED_PRODUCTS='@';



    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getItems($dataUpdateTime = null, array $ids = [], bool $isFullInfo = false)
    {
        $itemSelection = new ItemsSelection();
        return $itemSelection->getProducts(new Product(),$dataUpdateTime,$ids,$isFullInfo);
    }

    public function getPopularProductsIdsArr(int $qty)
    {
        $productsIdArr = Product::where('published',true)->orderBy('popularity', 'desc')->get(['id'])->take($qty);
        return $productsIdArr->map(function ($item) {
            return  $item->id;
        });
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('thumbs')
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion('thumb')
                    ->width(100)
                    ->height(100);
            });
    }

    public function sluggable(): array {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function letter(){
        return $this->belongsTo(Letter::class, 'letter_id');
    }
    public function clothes(){
        return $this->belongsTo(Cloth::class, 'cloth_id');
    }
    public function categories(){
        return $this->belongsToMany(Category::class, 'category_product');
    }
    public function size(){
        return $this->belongsTo(Size::class, 'size_id');
    }
    public function size_clothes(){
        return $this->belongsTo(SizeCloth::class, 'size_clothes_id');
    }
    public function colors(){
        return $this->belongsToMany(Color::class, 'color_product');
    }

    public function products_size(){
        return $this->belongsToMany(Product::class, 'product_size', 'products_size_id');
    }
    public function products_size_clothes(){
        return $this->belongsToMany(Product::class, 'product_size_clothe', 'products_size_id');
    }
    public function products_recom(){
        return $this->belongsToMany(Product::class, 'product_recom', 'products_recom_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'product_tag');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getImagesAttribute($value){
        $readyValues=[];
        if($value === null) return $value;
        $value=json_decode($value);


        foreach ($value as $item){
            if($item !== null) {
                $readyValue=new \stdClass();
                $readyValue->image= '/'.$item;
                array_push($readyValues,$readyValue);
            }
        }
        return $readyValues;

    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */



    public function setImagesAttribute($value)
    {
        $attribute_name = "images";
        $imagesProductsProps=config('filesystems.disks.this_project.product');
        $watermarkProps=null;//config('filesystems.disks.this_project.watermark');
        $id=$this->id;
        $newNames = []; $new_value_json = []; $images = [];
        $value_json= json_decode($value);
        $imageService= new ImageService();

        if ($id===null){//если это добавление нового объекта, а не редактирование - нужно взять в таблице значение слудующего автоинкремента
            $statement = DB::select("SHOW TABLE STATUS LIKE '".$this->table."'");
            $nextId = $statement[0]->Auto_increment;
            $id=$nextId;
        }

        foreach ($value_json as $k=>$item){ // Создаём новый массив с учётом удалённых изображений
            if(empty($item->image)){
                $imageService->removeImageIfExists($item->image, $imagesProductsProps);
                unset($value_json[$k]);
            }
            else {
                array_push($new_value_json, $value_json[$k]);
            }
        }

        foreach ($new_value_json as $k=>$item){
            $k++;
            $newNames[$k]=$this::ID_PREFIX.$id.'-'.$k;

            if ($item->image && !strpos($item->image,ImageService::PATH)) {// если к нам зашла не картинка а url (так как файл был сохранен ранее то мы ничего не делаем, а если картинка - то заходим в функцию и обрабатываем )
                $imageService->removeImageIfExists($newNames[$k],$imagesProductsProps);
                $imageService->removeImageIfExists($newNames[$k],$imagesProductsProps, '/originals/');
                $image = $imageService->prepareItemImages($item->image,$newNames[$k],$imagesProductsProps,$watermarkProps);
                array_push($images, $image);
            }
            else if($item->image && !empty($item->image) && file_exists($item->image)){
                $image = $item->image[0] != '/' ? $item->image : substr($item->image, 1);
                $image_array = explode(".", $image);  //Разбивает строку файла на имя и раширение
                $new_image_url = substr_replace($image_array[0],$k,-1) . '.' . $image_array[1];//Меняет последний символ
                rename($image, $new_image_url);
                $image = $new_image_url;
                array_push($images, $image);
            }
        }
        $this->attributes[$attribute_name] = json_encode($images);

    }

    public function setPosterAttribute($value)
    {
        $attribute_name = "poster";
        $imagesProductsProps=config('filesystems.disks.this_project.product');
        $watermarkProps= null; //config('filesystems.disks.this_project.watermark');
        $id=$this->id;
        if ($id===null){//если это добавление нового объекта, а не редактирование - нужно взять в таблице значение слудующего автоинкремента
            $statement = DB::select("SHOW TABLE STATUS LIKE '".$this->table."'");
            $nextId = $statement[0]->Auto_increment;
            $id=$nextId;
        }
        $newName=$this::ID_PREFIX.$id.'-0';
        $imageService= new ImageService();

        //dd($newName);
        if ($value==null) {
            $this->attributes[$attribute_name] = null;
            $imageService->removeImageIfExists($newName,$imagesProductsProps);

            return;
        }
        if (!strpos($value,ImageService::PATH)) {// если к нам зашла не картинка а url (так как файл был сохранен ранее то мы ничего не делаем, а если картинка - то заходим в функцию и обрабатываем )
            $imageService->removeImageIfExists($newName,$imagesProductsProps);
            $imageService->removeImageIfExists($newName,$imagesProductsProps, '/originals/');
            $this->attributes[$attribute_name] = $imageService->prepareItemImages($value,$newName,$imagesProductsProps,$watermarkProps);
        }
    }



}


<?php

namespace App\Models;

use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class SizeCloth extends Model
{
    use CrudTrait;

    protected $table = 'size_clothes';
    protected $guarded = ['id'];
    public const ID_PREFIX='dim';
    public const CLIENT_FILE_ID='dim';

    public static function getItems($dataUpdateTime=null, array $ids=[], bool $isFullInfo = false){
        $itemSelection = new ItemsSelection();
        return $itemSelection->getItems(new SizeCloth(),$dataUpdateTime,$ids,$isFullInfo);
    }
}

<?php

namespace App\Models;

use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $table = 'colors';
    protected $guarded = ['id'];
    protected $translatable = ['name'];

    public const ID_PREFIX='c';
    public const CLIENT_FILE_ID='c';

    public static function getItems($dataUpdateTime=null, array $ids=[], bool $isFullInfo = false){
        $itemSelection = new ItemsSelection();
        return $itemSelection->getItems(new Color(),$dataUpdateTime,$ids,$isFullInfo);
    }

    public function getClass(){
        return $this->table;
    }
}

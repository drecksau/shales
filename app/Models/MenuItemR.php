<?php

namespace App\Models;

use App\Nest\HeaderSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class MenuItemR extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $table = 'menu_i_r';
    protected $guarded = ['id'];
    protected $translatable = ['name', 'link'];
    public const ID_PREFIX='LR';

    public function parent() {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public static function getItems( $dataUpdateTime=null, array $ids=[], bool $isFullInfo = false){
        $itemSelection = new HeaderSelection();
        return $itemSelection->getItems(new MenuItemR(),$dataUpdateTime,$ids,$isFullInfo);
    }
}


<?php

namespace App\Models;

use App\Nest\BlogSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class CatArticle extends Model
{
    use CrudTrait;
    use Sluggable, SluggableScopeHelpers;
    use HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'cat_articles';
    protected $guarded = ['id'];
    protected $translatable = ['name', 'description', 'meta_title', 'meta_description'];
    public const ID_PREFIX = 'bc';
    public const CLIENT_FILE_ID='bc';

    public function sluggable(): array {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public static function getItems($dataUpdateTime = null, array $ids = [], bool $isFullInfo = false)
    {
        $itemSelection = new BlogSelection();
        $categories = $itemSelection->getItems(new CatArticle(), $dataUpdateTime, $ids, $isFullInfo);
        $categories=collect($categories) ->sortBy('lft');
        $categories=$categories->where('vis',1);
        return $categories->toArray() ;
    }

    public function getClass(){
        return $this->table;
    }
    public function parent() {
        return $this->belongsTo(CatArticle::class, 'parent_id');
    }
}

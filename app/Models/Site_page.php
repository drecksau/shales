<?php

namespace App\Models;

use App\Nest\Images\ImageService;
use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Support\Facades\DB;

class Site_page extends Model
{
    use CrudTrait;
    use HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'site_pages';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $translatable = ['name', 'description','meta_title','meta_description'];
    public const ID_PREFIX='page';
    public const CLIENT_FILE_ID='page';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getItems($dataUpdateTime=null, array $ids=[], bool $isFullInfo = false){
        $itemSelection= new ItemsSelection();
        $sitePages=$itemSelection->getItems(new Site_Page(),$dataUpdateTime,$ids,$isFullInfo);
        $sitePages=collect($sitePages) ->sortBy('lft');
        $sitePages=$sitePages->where('vis',1);
        return $sitePages->toArray();
    }

    public function scopeFirstLevelItems($query)
    {
        return $query->where('depth', '1')
            ->orWhere('depth', null)
            ->orderBy('lft', 'ASC');
    }
    public function setCoverAttribute($value)
    {
        $attribute_name = "cover";
        $imagesProductsProps=config('filesystems.disks.this_project.cover_pages');
        $watermarkProps= null; //config('filesystems.disks.this_project.watermark');
        $id=$this->id;
        if ($id===null){//если это добавление нового объекта, а не редактирование - нужно взять в таблице значение слудующего автоинкремента
            $statement = DB::select("SHOW TABLE STATUS LIKE '".$this->table."'");
            $nextId = $statement[0]->Auto_increment;
            $id=$nextId;
        }
        $newName=$this::ID_PREFIX.$id;
        $imageService= new ImageService();

        //dd($newName);
        if ($value==null) {
            $this->attributes[$attribute_name] = null;
            $imageService->removeImageIfExists($newName,$imagesProductsProps);

            return;
        }
        if (!strpos($value,ImageService::PATH)) {// если к нам зашла не картинка а url (так как файл был сохранен ранее то мы ничего не делаем, а если картинка - то заходим в функцию и обрабатываем )
            $imageService->removeImageIfExists($newName,$imagesProductsProps);
            $imageService->removeImageIfExists($newName,$imagesProductsProps, '/originals/');
            $this->attributes[$attribute_name] = $imageService->prepareItemImages($value,$newName,$imagesProductsProps,$watermarkProps);
        }
    }
}

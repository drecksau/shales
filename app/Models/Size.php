<?php

namespace App\Models;

use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    use CrudTrait;

    protected $table = 'sizes';
    protected $guarded = ['id'];

    public const ID_PREFIX='s';
    public const CLIENT_FILE_ID='size';

    public static function getItems($dataUpdateTime=null, array $ids=[], bool $isFullInfo = false){
        $itemSelection = new ItemsSelection();
        return $itemSelection->getItems(new Size(),$dataUpdateTime,$ids,$isFullInfo);
    }
}

<?php

namespace App\Models;

use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Cloth extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $table = 'clothes';
    protected $guarded = ['id'];
    protected $translatable = ['name', 'consist'];

    public const ID_PREFIX='cl';
    public const CLIENT_FILE_ID='cl';

    public static function getItems($dataUpdateTime=null, array $ids=[], bool $isFullInfo = false){
        $itemSelection = new ItemsSelection();
        return $itemSelection->getItems(new Cloth(),$dataUpdateTime,$ids,$isFullInfo);
    }

}

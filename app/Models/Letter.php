<?php

namespace App\Models;

use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Letter extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $table = 'letters';
    protected $guarded = ['id'];
    protected $translatable = ['name', 'meta_title', 'meta_description'];

    public const ID_PREFIX='l';
    public const CLIENT_FILE_ID='let';

    public static function getItems($dataUpdateTime=null, array $ids=[], bool $isFullInfo = false){
        $itemSelection = new ItemsSelection();
        return $itemSelection->getItems(new Letter(),$dataUpdateTime,$ids,$isFullInfo);
    }
    public function sluggable(): array {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}


<?php

namespace App\Models;

use App\Nest\BlogSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class TagArticle extends Model
{
    use CrudTrait;
    use HasTranslations;
    use Sluggable, SluggableScopeHelpers;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'tag_articles';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $translatable = ['name', 'description','meta_title','meta_description'];
    public const ID_PREFIX='t';
    public const CLIENT_FILE_ID='#';

    public static function getItems( $dataUpdateTime=null, array $ids=[], bool $isFullInfo = false){
        $itemSelection = new BlogSelection();
        return $itemSelection->getItems(new TagArticle(),$dataUpdateTime,$ids,$isFullInfo);
    }

    public static function prepareIdsForClient(array $tags){
        $preparedIds=[];
        foreach ($tags as $tag){
            $preparedId=Tag::ID_PREFIX.$tag->id;
            array_push($preparedIds,$preparedId);
        }
        return ($preparedIds===[])?null:$preparedIds;

    }

    public function sluggable(): array {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}

<?php

namespace App\Http\Controllers;

use App\Concord\Payment_sdk;
use App\Models\Order;
use App\Models\Temp_user;
use App\Models\User;
use App\ThirdPartyServices\SalesDrive;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Rate;

class OrdersController extends Controller
{
    public function createNewOrder(Request $request)
    {
        $validation=$this->validateOrder($request);
        if ($validation->fails()){
            return response($validation->errors());
        }

        $response = 'success';
        $salesDrive= new SalesDrive();
        $order = new Order();
        $email=$request->customerToken;
        $products=$salesDrive->prepareProductsToSalesDrive($request->products);
        $order->products = json_encode($products);
        $order->total = $request->total;
        $user = User::where('email', $email)->first();

        if ($user === null) {
            $user =  Temp_user::where('email', $email)->first();
            $order->temp_user_id=$user->id;
            $order->user_id=null;
        }
        else {
            $order->user_id=$user->id;
            $order->temp_user_id=null;
        }

        $requestInfo=$request->all();
        $order->order_info = json_encode($requestInfo);

        $order->save();

        $salesDrive->createOrder($products,
            $user->name,
            $user->last_name,
            $user->phone,
            $user->email,
            'Курьерская доставка',
            $request->shipping_destination,
            $user->city,
            'credit card',
            $order->id
        );

        $merchantForm = $this->concordOnMerchand($order);
        //$order->signature = $merchantForm['signature'];
        //$order->save();

        return $merchantForm['form'];

        return response($response);
    }

    private function validateOrder(Request $request){
        $validationRules=[
            'total' => 'required|numeric|min:100|max:1000000',
            'products'=>'required',
            'customerToken'=>'required|email',

        ];
        $validationMessages=[];
        $validator = Validator::make($request->all(),
            $validationRules,
            $validationMessages);
        return $validator;
    }

    public function concordResponse(Request $request)
    {
        $bankResponse = $request->all();
        $order = Order::where('id', $bankResponse['orderReference'])->first();
        $order->all_concord_details = json_encode($bankResponse);
        $order->save();

        if ($bankResponse['transactionStatus'] === 'Approved') {
            $this->sendOrderToCRm(json_decode($order->all_order_details));
            $this->sendDetailsMail(json_decode($order->all_order_details), $order->id);
            return;
        }

    }

    private function concordOnMerchand(Order $order)
    {
        $PRIVATE_KEY = '706w4tw4XNmi423r8Yq495IA1q653e5Tnj5g8tSwGdU052rpfa0i92mhcHT2Kew7v5kn0L4Xm9sedK9Hrf57O8H3fY7uxDkdXWJVHm3Sguf8mD4fiNIRXh5qJkJr642G';
        $MERCHANT_ID = 'k3X30m6RXlDWkAjhmBL1fVPyn8g';
        $sdk = new Payment_sdk($PRIVATE_KEY);
        $products = (json_decode($order->order_info))->products;
        $description = '';
        foreach ($products as $product) {
            $description = $description . ' ' . $product->name . ' ';
        }
        $fullSum = $order->total;

        #----Оплата на платежной странице процессинга----------------------------------------------------------
        // сумма считаеться не безопсно. Ее нужно переделать после того как поменяем понятие продукт
        $params = [
            "operation" => "Purchase",
            "merchant_id" => $MERCHANT_ID,
            "amount" => round($fullSum * $this->getCurrencyRate()),
            "order_id" => $order->id+100045878,
            "currency_iso" => "UAH",
            "description" => $description,
            "approve_url" => "http://alefbetsilk.com/thank-you",
            "decline_url" => "https://back.alefbetsilk.com/api/concord-response",
            "callback_url" => "https://back.alefbetsilk.com/api/concord-response",
            "cancel_url" => "https://alefbetsilk.com/"
        ];
        $html = $sdk->purchase($params);
        $dom = new \DOMDocument();
        $dom->loadHTML($html);
        $xpath = new \DOMXPath($dom);
        $tags = $xpath->query('//input[@name="signature"]');
        $signature = '';
        foreach ($tags as $tag) {
            $signature = trim($tag->getAttribute('value'));
            break;
        }
        return [
            'form' => $sdk->purchase($params),
            'signature' => $signature];
    }

    public function getCurrencyRate($currency = 'EUR')
    {
        return Rate::where('ccy', $currency)->first(['sell'])->sell;

    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Site_page;
use App\Nest\Service\DbToFileWizard;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    public function __construct(Request $request)
    {
        if (in_array($request->lang, config('app.available_locales'))) {
            app()->setLocale($request->lang);
        }
    }

    public function getNav(){
        $filePath = DbToFileWizard::dirName . app()->getLocale() . '/' . DbToFileWizard::navName;
        $mainArray = Storage::get($filePath);
        return response($mainArray, 200);
    }

    public function getDb()
    {
        $filePath = DbToFileWizard::dirName . app()->getLocale() . '/' . DbToFileWizard::fileName;
        $mainArray = Storage::get($filePath);
        return response($mainArray, 200);
    }

    public function init(Request $request)
    {
        if ($request->id === null) {
            $filePath = DbToFileWizard::dirName . app()->getLocale() . '/' . DbToFileWizard::initFileName;
            if (Storage::exists($filePath)) {
                $file = Storage::get($filePath);
                return response($file, 200);
            } else {
                return response('Файл не сгенерирован', 200);
            }
        } else {
            $fileDb = new DbToFileWizard();
            $typeId = preg_replace("/[^a-z]+/", "", $request->id);
            $productsIds = $fileDb->getProductsFromProps($request->id);
            $productObj = new Product();
            $isPages = false;
            if ($typeId === Site_page::ID_PREFIX) {
                $productObj = new Site_page();
                $isPages = true;
            }
            $productsItems = ($productsIds !== []) ? $productObj->getItems(null, $productsIds, $fileDb->foolInfoForProductOrNot($request->id)) : [];
            $mainArray = $fileDb->getPropsFromProducts($productsItems, $fileDb->foolInfoForProductOrNot($request->id), $isPages, $request->id);
            return response($mainArray);
        }
    }

    public function getBlog()
    {
        $filePath = DbToFileWizard::dirName . app()->getLocale() . '/' . DbToFileWizard::blogName;
        if (Storage::exists($filePath)) {
            $mainArray = Storage::get($filePath);
            return response($mainArray, 200);
        } else {
            return response('Файл не сгенерирован', 200);
        }

    }

    public function getEntity($id)
    {
        $fileDb = new DbToFileWizard();
        $entity = $fileDb->getObj($id);
        return response($entity, 200);
    }
}

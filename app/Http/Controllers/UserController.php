<?php

namespace App\Http\Controllers;

use App\Models\Post_operator;
use App\Models\Temp_user;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function getCurrentUser(Request $request){
        $user=Auth::user();
        $responseUser=json_encode(User::prepareForOutDoor($user));
        return response($responseUser);
    }

    public function getOrCreateUser(Request $request){
        $validation=$this->validateOrder($request);
        if ($validation->fails()){
            return response($validation->errors());
        }
        $user = User::where('email', $request->email)->first();
        if ($user === null) {
            if ($request->password === null) { // если пароль не прислали то делаю временного пользователя
                $user = Temp_user::where('email', $request->email)->first();
                if ($user === null) {
                    $user = new Temp_user();
                }
            } else { //если прислали пароль то регистрирую пользователя
                $user = new User();
                $request->request->set('password', Hash::make($request->input('password')));
                $user->password = Hash::make($request->password);
            }
            $user->news = boolval($request->news);
            $user->email = $request->email;
            $user->last_name = $request->last_name;
            $user->name = $request->name;
            $user->city = $request->city;
            $user->zip = $request->zip;
            $user->country = $request->country;
            $user->phone = $request->phone;
            //$user->shipping_provider = Post_operator::getNameFromClientId($request->shipping_provider);
            $user->shipping_destination = $request->address;
            $user->address = $request->address;
            //$user->payment_method = $request->payment_method;
            $user->save();
        }
        return $user->email;
    }

    private function validateOrder(Request $request){
        $validationRules=[
            'name' => 'required|string|max:50',
            'last_name'=>'required|string|max:50',
            'email'=>'required|email',
            'phone'=>'required',
            'city'=>'required|max:50',
            /*'shipping_provider'=>'required',
            'shipping_destination'=>'required',
            'payment_method'=>'required',*/

        ];
        $validationMessages=[];
        $validator = Validator::make($request->all(),
            $validationRules,
            $validationMessages);
        return $validator;
    }

    public function getUser($email=''){
        if (!empty($email)){
            $user=Temp_user::where('email',$email)->first();
        }
        if ($user!==null){
            $user= new Temp_user();
        }
        $prepUser=User::prepareForOutDoor($user);
        $prepUser=json_encode($prepUser);
        return response($prepUser);
    }
}

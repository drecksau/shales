<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Nest\Service\DbToFileWizard;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function init(){
        $dbFile= new DbToFileWizard();
        $dbFile->getPopularItemsDb();
        $dbFile->getDb();
        /*$prod= new Product();
        return $prod->getItems();*/
    }
}

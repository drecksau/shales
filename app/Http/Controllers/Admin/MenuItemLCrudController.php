<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MenuItemLRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class MenuItemLCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MenuItemLCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\MenuItemL::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/menuiteml');
        CRUD::setEntityNameStrings('Пункт меню', 'Пункты меню');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([ 'name' => 'name', 'label' => "Название"]);
        $this->crud->addColumn([ 'name' => 'link', 'label' => "Ссылка"]);
        $this->crud->addColumn([ 'name' => 'target', 'label' => "Режим"]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(MenuItemLRequest::class);

        $name = [
            'type'            => 'text',
            'name'            => 'name',
            'label'           => 'Название',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $link = [
            'type'            => 'text',
            'name'            => 'link',
            'label'           => 'Ссылка',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $target = [
            'type'            => 'enum',
            'name'            => 'target',
            'label'           => 'Режим',
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $published = [
            'type'            => 'toggle',
            'name'            => 'published',
            'label'           => 'Показывать на сайте',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-4'],
            'default' => 1
        ];

        CRUD::addField($name);
        CRUD::addField($link);
        CRUD::addField($target);
        CRUD::addField($published);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupReorderOperation()
    {
        $this->crud->set('reorder.label', 'name');
        $this->crud->set('reorder.max_level', 2);
    }

    public function store()
    {
        $response = $this->traitStore();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getNav();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getNav();
        return $response;
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArticleRequest;
use App\Models\CatArticle;
use App\Models\TagArticle;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ArticleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArticleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Article::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/article');
        CRUD::setEntityNameStrings('Статья', 'Статьи');
        $this->crud->setTitle('Статьи'); // set the Title for the create action
        $this->crud->setHeading('Статьи'); // set the Heading for the create action
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns([
            [
                'name' => 'name',
                'label' => 'Название',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhere('name->ru', 'like', '%' . $searchTerm . '%');
                }
            ],
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ArticleRequest::class);

        $this->crud->setSubheading('добавить/редактировать'); // set the Subheading for the create action
        CRUD::addFields([
            [
                'type'    => 'relationship',
                'name'    => 'categories',
                'model'   => 'App\Models\CatArticle',
                'label'   => 'Категория',
                'entity'  => 'categories',
                'allows_multiple' => true,
                'wrapper' => ['class'=>'form-group col-md-12'],
                'tab'     => 'Общая информация',
                'attribute' => 'name',
                'inline_create' => [ 'entity' => 'catarticle'],
                'pivot'     => true,
                'options'   => (function ($query) {
                    return $query->orderBy('name', 'ASC')->get();
                }),
            ],
            [
                'type'=> 'relationship',
                'name'    => 'tags',
                'model'=> 'App\Models\TagArticle',
                'label' => 'Теги',
                'entity'  => 'tags',
                'allows_multiple' => true,
                'wrapper' => ['class'=>'form-group col-md-12'],
                'tab'     => 'Общая информация',
                'attribute' => 'name',
                'inline_create' => [ 'entity' => 'tagarticle'],
                'pivot'     => true,
                'options'   => (function ($query) {
                    return $query->orderBy('name', 'ASC')->get();
                }),
            ],
            ['name' => 'name', 'label' => 'Название', 'type'=>'text', 'wrapper' => ['class' => 'form-group col-md-12'], 'tab' => 'Общая информация'],
            ['name'=>'description','label'=>'Контент', 'type' => 'wysiwyg', 'tab' => 'Общая информация'],
            [
                'name' => 'published',
                'label' => 'Опубликовать',
                'type' => 'toggle',
                'view_namespace' => 'toggle-field-for-backpack::fields',
                'tab' => 'Общая информация'
            ],
            ['name' => "poster", 'label' => "Постер", 'type' => 'image', 'crop' => false, 'aspect_ratio' => 1, 'allows_null' => false, 'allows_multiple' => true, 'tab' => 'Фото'],

            //SEO
            ['name' => 'slug', 'label' => 'Slug', 'tab' => 'SEO'],
            ['name' => 'meta_title', 'type'=>'text', 'label' => 'Page title', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
            ['name' => 'meta_description', 'label' => 'Page description', 'type' => 'textarea', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public function store()
    {
        $response = $this->traitStore();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getBlog();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();

        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getBlog();
        return $response;
    }
    public  function fetchCategories() {
        return $this->fetch(CatArticle::class);
    }
    public  function fetchTags() {
        return $this->fetch(TagArticle::class);
    }
}

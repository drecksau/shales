<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ContactsRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ContactsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ContactsCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Contacts::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/contacts');
        CRUD::setEntityNameStrings('Контакты', 'Контакты');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([ 'name' => 'name', 'label' => "Заголовок"]);
        $this->crud->addColumn([ 'name' => 'phones', 'label' => "Телефоны"]);
        $this->crud->addColumn([ 'name' => 'emails', 'label' => "Emails"]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ContactsRequest::class);

        $name = [
            'type'            => 'text',
            'name'            => 'name',
            'label'           => 'Заголовок',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $description = [
            'type'            => 'ckeditor',
            'name'            => 'description',
            'label'           => 'Описание',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $published = [
            'type'            => 'toggle',
            'name'            => 'published',
            'label'           => 'Показывать на сайте',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-4'],
            'default' => 1
        ];
        $phones = [
            'type'            => 'text',
            'name'            => 'phones',
            'label'           => 'Телефоны',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Контакты',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        $emails = [
            'type'            => 'text',
            'name'            => 'emails',
            'label'           => 'Emails',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Контакты',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        $meta_title = [
            'type'            => 'text',
            'name'            => 'meta_title',
            'label'           => 'Meta title',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'SEO',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $meta_description = [
            'type'            => 'textarea',
            'name'            => 'meta_description',
            'label'           => 'Meta description',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'SEO',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $slug = [
            'type'            => 'text',
            'name'            => 'slug',
            'label'           => 'Slug',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'SEO',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];

        $map = [
            'name'  => 'address-input', // do not change this
            'type'  => 'customGoogleMaps', // do not change this
            'label' => "Местоположение",
            'hint'  => '',
            'tab'             => 'Контакты',
            'attributes' => [
                'class' => 'form-control map-input', // do not change this, add more classes if needed
            ],
            'view_namespace' => 'custom-google-maps-field-for-backpack::fields',
        ];

        CRUD::addField($name);
        CRUD::addField($description);
        CRUD::addField($published);
        CRUD::addField($phones);
        CRUD::addField($emails);
        CRUD::addField($map);
        CRUD::addField($meta_title);
        CRUD::addField($meta_description);
        CRUD::addField($slug);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

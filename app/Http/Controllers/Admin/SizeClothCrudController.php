<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SizeClothRequest;
use App\Models\SizeCloth;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SizeClothCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SizeClothCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\SizeCloth::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/sizecloth');
        CRUD::setEntityNameStrings('размер шали', 'размеры шали');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([ 'name' => 'height', 'label' => "Длина"]);
        $this->crud->addColumn([ 'name' => 'width', 'label' => "Ширина"]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SizeClothRequest::class);

        $height = [
            'type'            => 'number',
            'name'            => 'height',
            'label'           => 'Длина',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        $width = [
            'type'            => 'number',
            'name'            => 'width',
            'label'           => 'Ширина',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        CRUD::addField($height);
        CRUD::addField($width);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store()
    {
        $response = $this->traitStore();
        $this->updateLinksAndFiles();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();
        $this->updateLinksAndFiles();
        return $response;
    }

    private function updateLinksAndFiles(){
        $entry= $this->crud->getCurrentEntry();
        $dbFileWizard = new DbToFileWizard();
        if (!$entry['published']){
            $dbFileWizard->removeFromClientFiles(SizeCloth::ID_PREFIX.$entry['id']);
        }

        $dbFileWizard->getDb();
        $dbFileWizard->getPopularItemsDb();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Site_pageRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class Site_PageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class Site_pageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Site_page::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/site_page');
        CRUD::setEntityNameStrings('Страница', 'Страницы');
        $this->crud->setTitle('Страницы'); // set the Title for the create action
        $this->crud->setHeading('Страницы'); // set the Heading for the create action
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns([
            [
                'name' => 'name',
                'label' => 'Название',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhere('name->ru', 'like', '%' . $searchTerm . '%');
                }
            ],
        ]);
    }

    protected function setupReorderOperation()
    {
        CRUD::set('reorder.label', 'name');
        CRUD::set('reorder.max_level', 1);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setSubheading('добавить/редактировать'); // set the Subheading for the create action
        CRUD::setValidation(Site_pageRequest::class);
        CRUD::addFields([
            [
                'name' => 'published',
                'label' => 'Опубликовать',
                'type' => 'toggle',
                'view_namespace' => 'toggle-field-for-backpack::fields',
                'tab' => 'Общая информация'
            ],
            ['name' => 'name', 'label' => 'Название', 'type'=>'text', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'Общая информация'],
            ['name'=>'description','label'=>'Контент', 'type' => 'wysiwyg', 'tab' => 'Общая информация'],
            ['name' => "cover", 'label' => "Постер", 'type' => 'image', 'crop' => false, 'aspect_ratio' => 1, 'allows_null' => false, 'allows_multiple' => true, 'tab' => 'Фото'],

            //SEO
            ['name' => 'slug', 'label' => 'Slug', 'tab' => 'SEO'],
            ['name' => 'meta_title', 'type'=>'text', 'label' => 'Page title', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
            ['name' => 'meta_description', 'label' => 'Page description', 'type' => 'textarea', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
        ]);

    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store()
    {
        $response = $this->traitStore();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        $dbFileWizard->getPopularItemsDb();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();

        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        $dbFileWizard->getPopularItemsDb();
        return $response;
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CharRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CharCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CharCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Char::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/char');
        CRUD::setEntityNameStrings('характеристика', 'Характеристики');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([ 'name' => 'name', 'label' => "Заголовок"]);
        $this->crud->addColumn([ 'name' => 'created_at', 'label' => "Дата публикации"]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CharRequest::class);

        $title = [
            'type'            => 'text',
            'name'            => 'name',
            'label'           => 'Название',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        $code_name = [
            'type'            => 'text',
            'name'            => 'code_name',
            'label'           => 'Кодовое название',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        $suffix = [
            'type'            => 'text',
            'name'            => 'suffix',
            'label'           => 'Суффикс',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        $type = [
            'type'            => 'enum',
            'name'            => 'type',
            'label'           => 'Тип',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        CRUD::addField($title);
        CRUD::addField($code_name);
        CRUD::addField($suffix);
        CRUD::addField($type);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\Category;
use App\Models\Product;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class ProductsCategoryChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductsCategoryChartController extends ChartController
{
    public function setup()
    {
        $colors=[];$rgbColor=[];
        $this->chart = new Chart();

        $this->chart->displayAxes(false);
        $this->chart->displayLegend(true);

        $labels = [];
        $dataset = [];
        $categories = Category::get()->toArray();
        foreach ($categories as $k=>$category) {
            $labels[] = $category['name']['ru'];
            $dataset[] = Product::join('category_product','category_product.product_id', 'products.id')->where('category_id', $category['id'])->get()->count();
        }
        $this->chart->labels($labels);

        for($i=1; $i<20; $i++) {
            foreach(array('r', 'g', 'b') as $color){
                $rgbColor[$color] = mt_rand(0, 255);
            }
            $colors[] = 'rgb('.implode(",", $rgbColor).')';
        }
        $this->chart->dataset('Red', 'pie', $dataset)
            ->backgroundColor($colors);
    }
}

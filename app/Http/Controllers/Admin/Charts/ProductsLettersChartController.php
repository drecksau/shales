<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\Category;
use App\Models\Letter;
use App\Models\Product;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class ProductsLettersChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductsLettersChartController extends ChartController
{
    public function setup()
    {
        $colors=[];$rgbColor=[];
        $this->chart = new Chart();

        $this->chart->displayAxes(false);
        $this->chart->displayLegend(true);

        $labels = [];
        $dataset = [];
        $letters = Letter::get()->toArray();
        foreach ($letters as $k=>$letter) {
            $labels[] = $letter['letter'];
            $dataset[] = Product::where('letter_id', $letter['id'])->get()->count();
        }
        $this->chart->labels($labels);

        for($i=1; $i<20; $i++) {
            foreach(array('r', 'g', 'b') as $color){
                $rgbColor[$color] = mt_rand(0, 255);
            }
            $colors[] = 'rgb('.implode(",", $rgbColor).')';
        }
        $this->chart->dataset('Red', 'pie', $dataset)
            ->backgroundColor($colors);
    }
}

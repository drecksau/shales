<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\Category;
use App\Models\Cloth;
use App\Models\Letter;
use App\Models\Product;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class ProductsChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductsChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        // MANDATORY. Set the labels for the dataset points
        $labels = [];
        for ($days_backwards = 30; $days_backwards >= 0; $days_backwards--) {
            if ($days_backwards == 0) {
                $labels[] = 'Сегодня';
            }
            else {
                $labels[] = $days_backwards.' дней назад';
            }

        }
        $this->chart->labels($labels);

        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/products'));

        // OPTIONAL
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
     public function data()
     {
         for ($days_backwards = 30; $days_backwards >= 0; $days_backwards--) {

             $products[] = Product::whereDate('created_at', today()->subDays($days_backwards))->count();
             $categories[] = Category::whereDate('created_at', today()->subDays($days_backwards))->count();
             $clothes[] = Cloth::whereDate('created_at', today()->subDays($days_backwards))->count();
             $letters[] = Letter::whereDate('created_at', today()->subDays($days_backwards))->count();
        }

         $this->chart->dataset('Товар', 'line', $products)
             ->color('rgb(77, 189, 116)')
             ->backgroundColor('rgba(77, 189, 116, 0.4)');

         $this->chart->dataset('Шали', 'line', $clothes)
             ->color('rgb(253, 150, 68)')
             ->backgroundColor('rgba(253, 150, 68, 0.4)');

         $this->chart->dataset('Категории', 'line', $categories)
             ->color('rgb(255, 193, 7)')
             ->backgroundColor('rgba(255, 193, 7, 0.4)');

         $this->chart->dataset('Буквы', 'line', $letters)
             ->color('rgba(70, 127, 208, 1)')
             ->backgroundColor('rgba(70, 127, 208, 0.4)');
     }
}

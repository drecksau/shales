<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LetterRequest;
use App\Models\Letter;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class LetterCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LetterCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Letter::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/letter');
        CRUD::setEntityNameStrings('буква', 'буквы');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([ 'name' => 'letter', 'label' => "Буква"]);
        $this->crud->addColumn([ 'name' => 'name', 'label' => "Название"]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(LetterRequest::class);

        $letter = [
            'type'            => 'text',
            'name'            => 'letter',
            'label'           => 'Буква',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        $name = [
            'type'            => 'text',
            'name'            => 'name',
            'label'           => 'Название',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        $meta_title = [
            'type'            => 'text',
            'name'            => 'meta_title',
            'label'           => 'Meta title',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'SEO',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        $meta_description = [
            'type'            => 'textarea',
            'name'            => 'meta_description',
            'label'           => 'Meta description',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'SEO',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $slug = [
            'type'            => 'text',
            'name'            => 'slug',
            'label'           => 'Slug',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'SEO',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        CRUD::addField($letter);
        CRUD::addField($name);
        CRUD::addField($slug);
        CRUD::addField($meta_title);
        CRUD::addField($meta_description);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store()
    {
        $response = $this->traitStore();
        $this->updateLinksAndFiles();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();
        $this->updateLinksAndFiles();
        return $response;
    }

    private function updateLinksAndFiles(){
        $entry= $this->crud->getCurrentEntry();
        $dbFileWizard = new DbToFileWizard();
        if (!$entry['published']){
            $dbFileWizard->removeFromClientFiles(Letter::ID_PREFIX.$entry['id']);
        }

        $dbFileWizard->getDb();
        $dbFileWizard->getPopularItemsDb();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CatArticleRequest;
use App\Models\CatArticle;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CatArticleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CatArticleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\CatArticle::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/catarticle');
        CRUD::setEntityNameStrings('категорию', 'Категории');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([ 'name' => 'name', 'label' => "Заголовок"]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CatArticleRequest::class);

        $name = [
            'type'            => 'text',
            'name'            => 'name',
            'label'           => 'Заголовок',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $parent_id = [
            'label' => 'Parent',
            'type' => 'select',
            'name' => 'parent_id',
            'entity' => 'parent',
            'attribute' => 'name',
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $slug = [
            'type'            => 'text',
            'name'            => 'slug',
            'label'           => 'Slug',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'SEO',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $description = [
            'type'            => 'ckeditor',
            'name'            => 'description',
            'label'           => 'Описание (SEO-текст)',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'SEO',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $published = [
            'type'            => 'toggle',
            'name'            => 'published',
            'label'           => 'Показывать на сайте',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-4'],
            'default' => 1
        ];
        $meta_title = [
            'type'            => 'text',
            'name'            => 'meta_title',
            'label'           => 'Meta title',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'SEO',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $meta_description = [
            'type'            => 'textarea',
            'name'            => 'meta_description',
            'label'           => 'Meta description',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'SEO',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];

        CRUD::addField($name);
        CRUD::addField($parent_id);
        CRUD::addField($published);
        CRUD::addField($slug);
        CRUD::addField($description);
        CRUD::addField($meta_title);
        CRUD::addField($meta_description);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    protected function setupReorderOperation()
    {
        $this->crud->set('reorder.label', 'name');
        $this->crud->set('reorder.max_level', 2);
    }

    public function store()
    {
        $response = $this->traitStore();
        $this->updateLinksAndFiles();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();
        $this->updateLinksAndFiles();
        return $response;
    }

    private function updateLinksAndFiles(){
        $entry= $this->crud->getCurrentEntry();
        $dbFileWizard = new DbToFileWizard();
        if (!$entry['published']){
            $dbFileWizard->removeFromClientFiles(CatArticle::ID_PREFIX.$entry['id']);
        }

        $dbFileWizard->getBlog();
    }
}

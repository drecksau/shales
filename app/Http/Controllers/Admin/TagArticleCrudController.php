<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TagArticleRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TagArticleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TagArticleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation{
        show as traitShow;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\TagArticle::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/tagarticle');
        CRUD::setEntityNameStrings('Тег', 'Теги');
        $this->crud->setTitle('Теги'); // set the Title for the create action
        $this->crud->setHeading('Теги'); // set the Heading for the create action
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns([
            [
                'name' => 'name',
                'label' => 'Название',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhere('name->ru', 'like', '%' . $searchTerm . '%');
                }
            ],
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setSubheading('добавить/редактировать'); // set the Subheading for the create action
        CRUD::setValidation(TagArticleRequest::class);

        CRUD::addFields([
            ['name' => 'name','type'=>'text', 'label' => 'Название', 'wrapper' => ['class' => 'form-group col-md-12'],'tab' => 'Общая информация'],
            //SEO
            ['name' => 'slug', 'label' => 'Slug', 'tab' => 'SEO'],
            ['name' => 'meta_title', 'type'=>'text', 'label' => 'Meta title', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
            ['name' => 'meta_description', 'label' => 'Meta description', 'type' => 'textarea', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
            ['name'=>'description','label'=>'Описание Страницы (SEO тексты)', 'type' => 'wysiwyg', 'tab' => 'SEO'],
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function show($id)
    {
        CRUD::addColumns([
            ['name' => 'name', 'label' => 'Название'],
        ]);
        $content = $this->traitShow($id);
        return $content;
    }

    public function store()
    {
        $response = $this->traitStore();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getBlog();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();

        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getBlog();
        return $response;
    }
}

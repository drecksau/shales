<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Cloth;
use App\Models\Color;
use App\Models\Letter;
use App\Models\Product;
use App\Models\Size;
use App\Models\SizeCloth;
use App\Models\Tag;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Gaspertrix\Backpack\DropzoneField\Traits\HandleAjaxMedia;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use HandleAjaxMedia;
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Product::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/product');
        CRUD::setEntityNameStrings('продукт', 'Продукция');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([ 'name' => 'sku', 'label' => "SKU"]);
        $this->crud->addColumn([ 'name' => 'name', 'label' => "Заголовок"]);
        $this->crud->addColumn([ 'name' => 'price', 'label' => "Цена"]);
        $this->crud->addColumn([ 'name' => 'sex', 'label' => "Пол", ]);
        /*$this->crud->addColumn([ 'name' => 'categories', 'label' => "Категория", ]);*/
        $this->crud->addColumn([ 'name' => 'created_at', 'label' => "Дата публикации"]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ProductRequest::class);

        $name = [
            'type'            => 'text',
            'name'            => 'name',
            'label'           => 'Заголовок',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        $code = [
            'type'            => 'text',
            'name'            => 'sku',
            'label'           => 'SKU',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-3'],
        ];
        $barcode = [
            'type'            => 'text',
            'name'            => 'barcode',
            'label'           => 'Штрихкод',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-3'],
        ];
        $quantity = [
            'type'            => 'number',
            'name'            => 'quantity',
            'label'           => 'Количество',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-3'],
        ];
        $popularity = [
            'type'            => 'number',
            'name'            => 'popularity',
            'label'           => 'Популярность',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-3'],
        ];
        $price = [
            'type'            => 'number',
            'name'            => 'price',
            'label'           => 'Цена',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'attributes' => [
                "step" => "any",
            ],
            'wrapper'         => ['class' => 'form-group col-md-3'],
        ];
        $old_price = [
            'type'            => 'number',
            'name'            => 'old_price',
            'label'           => 'Старая цена',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'attributes' => [
                "step" => "any",
            ],
            'wrapper'         => ['class' => 'form-group col-md-3'],
        ];
        $sex = [
            'type'            => 'enum',
            'name'            => 'sex',
            'label'           => 'Пол',
            'allows_null'     => false,
            'allows_multiple' => false,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-3'],
        ];
        $description = [
            'type'            => 'ckeditor',
            'name'            => 'description',
            'label'           => 'Описание',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $published = [
            'type'            => 'toggle',
            'name'            => 'published',
            'label'           => 'Показывать на сайте',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-4'],
            'default' => 1
        ];
        $poster = [
            'label' => "Постер",
            'name' => "poster",
            'type' => 'image',
            'crop' => false,
            'aspect_ratio' => 1,
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'          => 'Фото',
        ];

        $photos = [
            'label' => 'Photos',
            'type' => 'image',
            'name' => 'photos',
            'collection' => 'photos',
            'thumb_collection' => 'thumb',
            'options' => [
                'thumbnailHeight' => 120,
                'thumbnailWidth' => 120,
                'maxFilesize' => 10,
                'addRemoveLinks' => true,
                'createImageThumbnails' => true,
            ],
        ];
        $images = [
            'name'  => 'images',
            'label' => 'Images',
            'type'  => 'repeatable',
            'sortable'   => true,
            'fields' => [
                [
                    'label' => "Фото",
                    'name' => "image",
                    'type' => 'image',
                    'crop' => false,
                    'aspect_ratio' => 1,
                ],
            ],
            'new_item_label'  => 'Добавить фото',
            'init_rows' => 1,
            'allows_null'     => true,
            'allows_multiple' => true,
            'tab'          => 'Фото',

        ];
        $category = [
            'type'    => 'relationship',
            'name'    => 'categories',
            'model'   => 'App\Models\Category',
            'label'   => 'Категория',
            'entity'  => 'categories',
            'allows_multiple' => true,
            'wrapper' => ['class'=>'form-group col-md-9'],
            'tab'     => 'Основное',
            'attribute' => 'name',
            'inline_create' => [ 'entity' => 'category'],
            'pivot'     => true,
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
        ];
        $letter = [
            'type'=> 'select2',
            'name'    => 'letter',
            'model'=> 'App\Models\Letter',
            'label' => 'Буква',
            'wrapper' => ['class'=>'form-group col-md-6'],
            'tab' => 'Основное',
            'attribute' => 'name',
        ];
        $clothes = [
            'type'=> 'relationship',
            'name'    => 'clothes',
            'model'=> 'App\Models\Cloth',
            'label' => 'Ткань',
            'wrapper' => ['class'=>'form-group col-md-6'],
            'tab' => 'Основное',
            'attribute' => 'name',
            'inline_create' => [ 'entity' =>'cloth']
        ];
        $size = [
            'type'=> 'relationship',
            'name'    => 'size',
            'model'=> 'App\Models\Size',
            'label' => 'Обычный размер',
            'wrapper' => ['class'=>'form-group col-md-6'],
            'tab' => 'Основное',
            'attribute' => 'name',
            'inline_create' => [ 'entity' =>'size']
        ];
        $colors = [
            'type'=> 'relationship',
            'name'    => 'colors',
            'model'=> 'App\Models\Color',
            'label' => 'Цвет',
            'entity'  => 'colors',
            'allows_multiple' => true,
            'wrapper' => ['class'=>'form-group col-md-6'],
            'tab'     => 'Основное',
            'attribute' => 'name',
            'inline_create' => [ 'entity' => 'color'],
            'pivot'     => true,
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
        ];
        $tags = [
            'type'=> 'relationship',
            'name'    => 'tags',
            'model'=> 'App\Models\Tag',
            'label' => 'Теги',
            'entity'  => 'tags',
            'allows_multiple' => true,
            'wrapper' => ['class'=>'form-group col-md-6'],
            'tab'     => 'Основное',
            'attribute' => 'name',
            'inline_create' => [ 'entity' => 'tag'],
            'pivot'     => true,
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
        ];
        $size_clothe = [
            'type'=> 'relationship',
            'name'    => 'size_clothes',
            'model'=> 'App\Models\SizeCloth',
            'label' => 'Размер шалей',
            'wrapper' => ['class'=>'form-group col-md-6'],
            'tab' => 'Основное',
            'attribute' => 'height',
            'inline_create' => [ 'entity' =>'sizecloth']
        ];

        $meta_title = [
            'type'            => 'text',
            'name'            => 'meta_title',
            'label'           => 'Meta title',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'SEO',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        $meta_description = [
            'type'            => 'textarea',
            'name'            => 'meta_description',
            'label'           => 'Meta description',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'SEO',
            'wrapper'         => ['class' => 'form-group col-md-12'],
        ];
        $slug = [
            'type'            => 'text',
            'name'            => 'slug',
            'label'           => 'Slug',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'SEO',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        $products_by_size = [
            'type'=> 'relationship',
            'name'    => 'products_size',
            'entity'  => 'products_size',
            'allows_multiple' => true,
            'model'=> 'App\Models\Product',
            'label' => 'Связанные товары по размерам',
            'wrapper' => ['class'=>'form-group col-md-12'],
            'tab' => 'Связанные товары',
            'attribute' => 'name',
            'inline_create' => [ 'entity' =>'product'],
            'pivot'     => true,
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
        ];
        $products_by_size_cl = [
            'type'=> 'relationship',
            'name'    => 'products_size_clothes',
            'entity'  => 'products_size_clothes',
            'allows_multiple' => true,
            'model'=> 'App\Models\Product',
            'label' => 'Связанные товары по материалам',
            'wrapper' => ['class'=>'form-group col-md-12'],
            'tab' => 'Связанные товары',
            'attribute' => 'name',
            'inline_create' => [ 'entity' =>'product'],
            'pivot'     => true,
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
        ];
        $products_recom = [
            'type'=> 'relationship',
            'name'    => 'products_recom',
            'entity'  => 'products_recom',
            'allows_multiple' => true,
            'model'=> 'App\Models\Product',
            'label' => 'Рекомендуемые товары',
            'wrapper' => ['class'=>'form-group col-md-12'],
            'tab' => 'Связанные товары',
            'attribute' => 'name',
            'inline_create' => [ 'entity' =>'product'],
            'pivot'     => true,
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
        ];

        CRUD::addField($name);


        CRUD::addField($code);
        CRUD::addField($barcode);
        CRUD::addField($quantity);
        CRUD::addField($price);
        CRUD::addField($old_price);
        CRUD::addField($sex);
        CRUD::addField($popularity);


        CRUD::addField($category);
        CRUD::addField($clothes);
        CRUD::addField($letter);
        CRUD::addField($colors);
        CRUD::addField($size);
        CRUD::addField($size_clothe);
        CRUD::addField($tags);

        CRUD::addField($description);
        CRUD::addField($published);


        //CRUD::addField($materials);

        //CRUD::addField($photos);
        CRUD::addField($poster);
        CRUD::addField($images);


        CRUD::addField($products_by_size);
        CRUD::addField($products_by_size_cl);
        CRUD::addField($products_recom);

        CRUD::addField($meta_title);
        CRUD::addField($slug);
        CRUD::addField($meta_description);

        $this->data['widgets']['after_content'] = [
            [
                'type' => 'checkboxes'
            ]
        ];
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public  function fetchLetter() {
        return $this->fetch(Letter::class);
    }
    public  function fetchClothes() {
        return $this->fetch(Cloth::class);
    }
    public  function fetchCategories() {
        return $this->fetch(Category::class);
    }
    public  function fetchSize() {
        return $this->fetch(Size::class);
    }
    public  function fetchSize_clothes() {
        return $this->fetch(SizeCloth::class);
    }
    public  function fetchColors() {
        return $this->fetch(Color::class);
    }
    public  function fetchTags() {
        return $this->fetch(Tag::class);
    }
    public  function fetchProducts_size() {
        return $this->fetch(Product::class);
    }
    public  function fetchProducts_size_clothes() {
        return $this->fetch(Product::class);
    }
    public  function fetchProducts_recom() {
        return $this->fetch(Product::class);
    }

    public function store()
    {
        $response = $this->traitStore();
        $this->updateLinksAndFiles();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();
        $this->updateLinksAndFiles();
        return $response;
    }

    private function updateLinksAndFiles(){
        $entry= $this->crud->getCurrentEntry();
        $dbFileWizard = new DbToFileWizard();
        if (!$entry['published']){
            $dbFileWizard->removeFromClientFiles(Product::ID_PREFIX.$entry['id']);
        }

        $dbFileWizard->getDb();
        $dbFileWizard->getPopularItemsDb();
    }
}

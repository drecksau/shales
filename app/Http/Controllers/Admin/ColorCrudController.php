<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ColorRequest;
use App\Models\Color;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ColorCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ColorCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Color::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/color');
        CRUD::setEntityNameStrings('цвет', 'цвета');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([ 'name' => 'name', 'label' => "Название"]);
        $this->crud->addColumn([ 'name' => 'color', 'label' => "Цвет"]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ColorRequest::class);

        $name = [
            'type'            => 'text',
            'name'            => 'name',
            'label'           => 'Название',
            'allows_null'     => false,
            'allows_multiple' => true,
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-6'],
        ];
        $color = [
            'type'            => 'color_picker',
            'name'            => 'color',
            'label'           => 'Цвет',
            'default'         => '#000000',
            'tab'             => 'Основное',
            'wrapper'         => ['class' => 'form-group col-md-6'],
            'color_picker_options' => [
                'customClass' => 'custom-class',
                'horizontal' => true,
                'extensions' => [
                    [
                        'name' => 'swatches', // extension name to load
                        'options' => [ // extension options
                            'colors' => [
                                'primary' => '#337ab7',
                                'success' => '#5cb85c',
                                'info' => '#5bc0de',
                                'warning' => '#f0ad4e',
                                'danger' => '#d9534f'
                            ],
                            'namesAsValues' => false
                        ]
                    ]
                ]
            ]
        ];

        CRUD::addField($name);
        CRUD::addField($color);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store()
    {
        $response = $this->traitStore();
        $this->updateLinksAndFiles();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();
        $this->updateLinksAndFiles();
        return $response;
    }

    private function updateLinksAndFiles(){
        $entry= $this->crud->getCurrentEntry();
        $dbFileWizard = new DbToFileWizard();
        if (!$entry['published']){
            $dbFileWizard->removeFromClientFiles(Color::ID_PREFIX.$entry['id']);
        }

        $dbFileWizard->getDb();
        $dbFileWizard->getPopularItemsDb();
    }
}

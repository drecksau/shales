<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->char('role')->default('customer');
            $table->char('last_name')->nullable();
            $table->char('city')->nullable();
            $table->char('phone')->nullable();
            $table->json('shipping_method')->nullable();
            $table->char('shipping_provider')->nullable();
            $table->char('shipping_destination')->nullable();
            $table->char('payment_mask')->nullable();
            $table->char('payment_method')->nullable();
            $table->char('payment_provider')->nullable();
            $table->char('payment_tocken')->nullable();
            $table->boolean('news_subscribe')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role');
            $table->dropColumn('last_name');
            $table->dropColumn('city');
            $table->dropColumn('phone');
            $table->dropColumn('shipping_method');
            $table->dropColumn('shipping_provider');
            $table->dropColumn('shipping_destination');
            $table->dropColumn('payment_mask');
            $table->dropColumn('payment_method');
            $table->dropColumn('payment_provider');
            $table->dropColumn('payment_tocken');
            $table->dropColumn('news_subscribe');
        });
    }
}

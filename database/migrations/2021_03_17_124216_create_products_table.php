<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->json('title')->nullable();
            $table->json('description')->nullable(); // Description for product
            $table->string('price')->nullable();
            $table->string('old_price')->nullable();
            $table->string('sku')->nullable(); //vendor code
            $table->string('barcode')->nullable();
            $table->enum('sex', ['man', 'woman', 'uni'])->default('man');

            $table->string('quantity')->nullable();
            $table->string('popularity')->nullable();
            $table->string('poster')->nullable();
            $table->json('images')->nullable();

            $table->unsignedBigInteger('size_id')->nullable();
            $table->foreign('size_id')
                ->references('id')
                ->on('sizes')
                ->onDelete('cascade');

            $table->unsignedBigInteger('cloth_id')->nullable();
            $table->foreign('cloth_id')
                ->references('id')
                ->on('clothes')
                ->onDelete('cascade');

            $table->unsignedBigInteger('size_clothes_id')->nullable();
            $table->foreign('size_clothes_id')
                ->references('id')
                ->on('size_clothes')
                ->onDelete('cascade');

            $table->unsignedBigInteger('letter_id')->nullable();
            $table->foreign('letter_id')
                ->references('id')
                ->on('letters')
                ->onDelete('cascade');



            $table->boolean('published')->default(true);
            $table->string('slug')->nullable();
            $table->json('meta_title')->nullable();
            $table->json('meta_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

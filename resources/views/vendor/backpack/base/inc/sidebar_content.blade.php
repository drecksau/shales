<li class='nav-item'>
    <a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a>
</li>
...
<li class="nav-title">Контент</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> Магазин</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('letter') }}'><i class='nav-icon la la-bold'></i> Буквы</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('cloth') }}'><i class='nav-icon la la-ticket-alt'></i> Ткани</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('size') }}'><i class='nav-icon la la-ruler'></i> Стандартные размеры</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('sizecloth') }}'><i class='nav-icon la la-compress-arrows-alt'></i> Размеры для шали</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon la-folder-open'></i> Категории</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('char') }}'><i class='nav-icon la la-filter'></i> Характеристики</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('color') }}'><i class='nav-icon la la-palette'></i> Цвета</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class='nav-icon la la-flask'></i> Продукция</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('tag') }}'><i class="las la-hashtag"></i><i class="las la-wine-bottle"></i> Хештеги</a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="{{ backpack_url('article') }}"><i class="la la-blog nav-icon"></i> Блог</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('article') }}'><i class='nav-icon la la-file'></i> Статьи</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('catarticle') }}'><i class='nav-icon la la-folder-open'></i> Категории</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('tagarticle') }}'><i class='nav-icon la la-hashtag'></i> Теги</a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="{{ backpack_url('menuiteml') }}"><i class="la la-bars nav-icon"></i> Меню</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('menuiteml') }}'><i class='nav-icon la la-align-left'></i> Слева</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('menuitemr') }}'><i class='nav-icon la la-align-right'></i> Справа</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('menuitemf') }}'><i class='nav-icon la la-align-justify'></i> Подвал</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="{{ backpack_url('site_page') }}"><i class="la la-file-alt nav-icon"></i>Общие страницы</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('site_page') }}'><i class='nav-icon la la-file-alt'></i> Страницы сайта</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('contacts') }}'><i class='nav-icon la la-phone-volume'></i> Контакты</a></li>
    </ul>
</li>
...

<li class="nav-title">Администратору</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-house-damage"></i> Конфигурация</a>
    <ul class="nav-dropdown-items">
        {{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('redirect') }}'><i class='nav-icon la la-external-link-alt'></i> Редиректы</a></li>
        --}}
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('variable') }}'><i class='nav-icon la la-font'></i>Переменные</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('backup') }}'><i class='nav-icon la la-hdd-o'></i> Бекап данных</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('log') }}'><i class='nav-icon la la-terminal'></i> Логи</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}\"><i class="nav-icon la la-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Аутентификация</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Пользователи</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Роли</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Права</span></a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('temp_user') }}'><i class='nav-icon la la-user'></i> Незарег. пользователи</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('order') }}'><i class='nav-icon la la-question'></i>Копии заказов</a></li>
    </ul>
</li>

@extends(backpack_view('blank'))

@php

    $widgets['before_content'] = [[
        'type'         => 'jumbotron',
        'heading'     => '<h1>Добропожаловать на сайт!</h1>',
        'content'     => 'Используйте панель слева для создания, редактирования или удаления контента. Ниже расположены информативные графиики.',
        'button_link' => backpack_url('logout'),
        'button_text' => trans('backpack::base.logout'),

    ],
    [
        'type'    => 'div',
        'class'   => 'row',
        'content' => [
            [
                'type'        => 'progress',
                'class'       => 'card text-white bg-success',
                'value'       => \App\Models\Product::count(),
                'description' => 'Количество товаров.',
                'progress'    => 100, // integer
            ],
            [
                'type'        => 'progress',
                'class'       => 'card text-white bg-yellow',
                'value'       => \App\Models\Category::count(),
                'description' => 'Количество Категорий.',
                'progress'    => 100, // integer
            ],
            [
                'type'        => 'progress',
                'class'       => 'card text-white bg-orange',
                'value'       => \App\Models\Cloth::count(),
                'description' => 'Количество Шалей.',
                'progress'    => 100, // integer
            ],
            [
                'type'        => 'progress',
                'class'       => 'card text-white bg-blue',
                'value'       => \App\Models\Letter::count(),
                'description' => 'Количество Букв.',
                'progress'    => 100, // integer
            ],
        ]
    ], [
            'type'    => 'div',
            'class'   => 'row',
            'content' => [
                [
                    'type'        => 'chart',
                    'class'   => 'card',
                     'wrapper' => ['class'=> 'col-md-6'] ,
                     'content' => [
                      'header' => '<h4>Количество товара в категориях</h4>',
                      'body'   => '',
                     ],
                    'controller' => \App\Http\Controllers\Admin\Charts\ProductsCategoryChartController::class,
                ],
                [
                    'type'        => 'chart',
                    'class'   => 'card',
                     'wrapper' => ['class'=> 'col-md-6'] ,
                     'content' => [
                      'header' => '<h4>Количество товара по буквам</h4>',
                      'body'   => '',
                     ],
                    'controller' => \App\Http\Controllers\Admin\Charts\ProductsLettersChartController::class,
                ]
            ]
    ], [
        'type'        => 'chart',
        'class'   => 'card',
         'wrapper' => ['class'=> ''] ,
         'content' => [
          'header' => '<h3>Контент</h3>',
          'body'   => 'График добавления контента за последние 30 дней <br><br>',
         ],
        'controller' => \App\Http\Controllers\Admin\Charts\ProductsChartController::class,
    ]];
@endphp

@section('content')
@endsection

<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () {
    Route::crud('letter', 'LetterCrudController');
    Route::crud('cloth', 'ClothCrudController');
    Route::crud('size', 'SizeCrudController');
    Route::crud('sizecloth', 'SizeClothCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('char', 'CharCrudController');
    Route::crud('product', 'ProductCrudController');
    Route::post('product/{id}/media', 'ProductCrudController@uploadMedia');
    Route::delete('product/{id}/media/{mediaId}', 'ProductCrudController@deleteMedia');
    Route::post('product/{id}/media/reorder', 'ProductCrudController@reorderMedia');
    Route::crud('color', 'ColorCrudController');
    Route::get('charts/products', 'Charts\ProductsChartController@response')->name('charts.products.index');
    Route::get('charts/products-category', 'Charts\ProductsCategoryChartController@response')->name('charts.products-category.index');
    Route::get('charts/products-letters', 'Charts\ProductsLettersChartController@response')->name('charts.products-letters.index');
    Route::crud('site_page', 'Site_pageCrudController');
    Route::crud('tag', 'TagCrudController');
    Route::crud('temp_user', 'Temp_userCrudController');
    Route::crud('order', 'OrderCrudController');
    Route::crud('article', 'ArticleCrudController');
    Route::crud('catarticle', 'CatArticleCrudController');
    Route::crud('tagarticle', 'TagArticleCrudController');
    Route::crud('menuitemr', 'MenuItemRCrudController');
    Route::crud('menuiteml', 'MenuItemLCrudController');
    Route::crud('contacts', 'ContactsCrudController');
    Route::crud('menuitemf', 'MenuItemFCrudController');
    Route::crud('variable', 'VariableCrudController');
});
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('db')->group(function (){
    Route::get('blog',[\App\Http\Controllers\ClientController::class,'getBlog']);
    Route::get('full',[\App\Http\Controllers\ClientController::class,'getDb']);
    Route::get('navigation',[\App\Http\Controllers\ClientController::class,'getNav']);
    Route::get('init/{id?}',[\App\Http\Controllers\ClientController::class,'init']);
    Route::get('/entity/{id}', [\App\Http\Controllers\ClientController::class,'getEntity']);
});



Route::prefix('test')->group(function (){
    Route::get('/init',[\App\Http\Controllers\TestController::class,'init']);
    Route::get('/privat', [\App\Miscellaneous\PrivatBankApi::class,'getRates']);
    Route::get('/blog',[\App\Nest\Service\DbToFileWizard::class,'getBlog']);
    Route::get('/nav',[\App\Nest\Service\DbToFileWizard::class,'getNav']);
});

Route::post('/create-order', [\App\Http\Controllers\OrdersController::class,'createNewOrder']);
Route::post('/set-user', [\App\Http\Controllers\UserController::class,'getOrCreateUser']);
Route::get('/get-user/{email?}', [\App\Http\Controllers\UserController::class,'getUser']);
Route::get('/get_mount', [\App\Http\Controllers\OrdersController::class,'getCurrencyRate']);
Route::any('/concord-response', [\App\Http\Controllers\OrdersController::class,'concordResponse']);
